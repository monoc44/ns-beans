package com.nannieshare;

import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

public class StringUtilsTest {

  @Test
  public void containsBlank() {
    Assert.assertFalse(StringUtils.containsBlank("test", "a", "B"));
    Assert.assertTrue(StringUtils.containsBlank("test", "", "B"));
    Assert.assertTrue(StringUtils.containsBlank("test", null, "B"));
    Assert.assertTrue(StringUtils.containsBlank((String) null));
    Assert.assertTrue(StringUtils.containsBlank());
  }

  @Test
  public void encodeBase64() throws UnsupportedEncodingException {
    String value = "email:foo@gmail.com;id:135";

    String encoded = StringUtils.encodeBase64(value);
    Assert.assertNotNull(encoded);

    String decoded = StringUtils.decodeBase64(encoded);
    Assert.assertNotNull(decoded);

    Assert.assertNotSame(encoded, decoded);
    Assert.assertEquals(value, decoded);
  }


  @Test
  public void encodeUrlParameter1() {
    String value = "eNeGqhzf+OEaQ/lQP+1fOIOM1EK/cmXP";
    String expectedResult = "eNeGqhzf%2BOEaQ%2FlQP%2B1fOIOM1EK%2FcmXP";
    Assert.assertEquals(expectedResult, StringUtils.encodeUrlParameter(value));
  }

  @Test
  public void encodeUrlParameter2() {
    String value = "ab%cd=ef";
    String expectedResult = "ab%25cd%3Def";
    Assert.assertEquals(expectedResult, StringUtils.encodeUrlParameter(value));
  }

}
