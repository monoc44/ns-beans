package com.nannieshare.enums.daycare;

import com.nannieshare.enums.map.ChildCareType;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DaycareTypeTest {

  @Test
  public void testFrom() {
    assertTrue(ChildCareType.from(3).equals(ChildCareType.SCHOOL_AGE_DAY_CARE_CENTER));
  }

}
