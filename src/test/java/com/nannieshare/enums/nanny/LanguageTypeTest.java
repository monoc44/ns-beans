package com.nannieshare.enums.nanny;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class LanguageTypeTest {

  @Test
  public void testAsProperties() {
    Assert.assertNotNull(LanguageType.asProperties());
    Assert.assertFalse(LanguageType.asProperties().isEmpty());
    for (Map.Entry<Integer, String> entry : LanguageType.asProperties().entrySet()) {
      LanguageType language = LanguageType.from(entry.getKey());
      Assert.assertNotNull(language);
      Assert.assertEquals(entry.getKey(), (Integer) language.id());
      Assert.assertNotSame(language.name(), entry.getValue());
      Assert.assertEquals(language.name(), entry.getValue().toUpperCase());
    }
  }
}
