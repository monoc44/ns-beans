package com.nannieshare.service

import com.nannieshare.beans.user.OpeningBean

import java.time.LocalDate

class OpeningServiceTest extends groovy.util.GroovyTestCase {

    // Used as test cases
    OpeningService.OpeningLoader loader = new OpeningService.OpeningLoader() {
        @Override
        List<OpeningBean> loadOpenings() {
            return Arrays.asList(
                    build("94", "2018/12/01"),
                    build("94705", "2018/01/01"),
                    build("95", "2019/01/01")
            )
        }
    }

    void testWithMissingZipCode() {
        def openingService = OpeningService.build(loader)
        def opening = openingService.getStatus(null)
        assertNull(opening)
    }

    void testZipCodeOpen1() {
        def openingService = OpeningService.build(loader, LocalDate.of(2018, 1, 1));
        def opening = openingService.getStatus("94705")
        assertNull(opening.getEtaInWeeks())
        assertTrue(opening.isOpen())
    }

    void testZipCodeOpen2() {
        def openingService = OpeningService.build(loader, LocalDate.of(2018, 8, 1));
        def opening = openingService.getStatus("94705")
        assertNull(opening.getEtaInWeeks())
        assertTrue(opening.isOpen())
    }

    void testZipCodeClose1() {
        def openingService = OpeningService.build(loader, LocalDate.of(2018, 11, 17));
        def opening = openingService.getStatus("94710")
        assertFalse(opening.isOpen())
        assertEquals(2, opening.getEtaInWeeks())
    }

    void testZipCodeClose2() {
        def openingService = OpeningService.build(loader, LocalDate.of(2018, 11, 24));
        def opening = openingService.getStatus("94710")
        assertFalse(opening.isOpen())
        assertEquals(1, opening.getEtaInWeeks())
    }

    void testZipCodeClose3() {
        def openingService = OpeningService.build(loader, LocalDate.of(2018, 11, 30));
        def opening = openingService.getStatus("94710")
        assertFalse(opening.isOpen())
        assertEquals(0, opening.getEtaInWeeks())
    }

    void testOpeningDate1() {
        def openingService = OpeningService.build(loader);
        def openingDate = openingService.getOpeningDate("94705")
        assertEquals(LocalDate.of(2018, 1, 1), openingDate)
    }

    void testOpeningDate2() {
        def openingService = OpeningService.build(loader);
        def openingDate = openingService.getOpeningDate("94000")
        assertEquals(LocalDate.of(2018, 12, 1), openingDate)
    }

    void testZipCodeNotFound() {
        def openingService = OpeningService.build(loader)
        def opening = openingService.getStatus("00000")
        assertNull(opening.etaInWeeks)
    }

    OpeningBean build(String zipCode, String openingDate){
        OpeningBean toReturn = new OpeningBean()
        toReturn.setOpeningDate(openingDate)
        toReturn.setZipCode(zipCode)
        return toReturn
    }
}
