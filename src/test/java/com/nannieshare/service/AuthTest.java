package com.nannieshare.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import static org.junit.Assert.*;

public class AuthTest {

  @Test
  public void isJWTSecured() {
    Assert.assertFalse(Auth.get().isJWTSecured());
  }

  @Test
  public void isJWTSecured2() {
    Authentication auth = new UsernamePasswordAuthenticationToken("automated-comm", "NannieShare rocks!");
    SecurityContext ctx = new SecurityContextImpl();
    ctx.setAuthentication(auth);
    SecurityContextHolder.setContext(ctx);
    Assert.assertFalse(Auth.get().isJWTSecured());
    SecurityContextHolder.createEmptyContext();
  }
}