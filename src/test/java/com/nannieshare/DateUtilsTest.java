package com.nannieshare;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;

public class DateUtilsTest {

  @Test
  public void testParseToLocalDate1() {
    LocalDate localDate = DateUtils.parseToLocalDate(null);
    Assert.assertNull(localDate);
  }

  @Test
  public void testParseToLocalDate2() {
    String value = "1978/08/03";
    LocalDate localDate = DateUtils.parseToLocalDate(value);
    Assert.assertNotNull(localDate);
    Assert.assertEquals(1978, localDate.get(ChronoField.YEAR));
    Assert.assertEquals(8, localDate.get(ChronoField.MONTH_OF_YEAR));
    Assert.assertEquals(3, localDate.get(ChronoField.DAY_OF_MONTH));
  }

  @Test
  public void testParseToLocalDate3() {
    String value = "2017/05/14 22:15:00";
    LocalDate localDate = DateUtils.parseToLocalDate(value);
    Assert.assertNotNull(localDate);
    Assert.assertEquals(2017, localDate.get(ChronoField.YEAR));
    Assert.assertEquals(5, localDate.get(ChronoField.MONTH_OF_YEAR));
    Assert.assertEquals(14, localDate.get(ChronoField.DAY_OF_MONTH));
  }

  @Test
  public void testParseToLocalDate4() {
    String value = "1978/04/03 10:14";
    LocalDate localDate = DateUtils.parseToLocalDate(value);
    Assert.assertNotNull(localDate);
    Assert.assertEquals(1978, localDate.get(ChronoField.YEAR));
    Assert.assertEquals(4, localDate.get(ChronoField.MONTH_OF_YEAR));
    Assert.assertEquals(3, localDate.get(ChronoField.DAY_OF_MONTH));
  }

  @Test(expected = DateTimeParseException.class)
  public void testParseToLocalDate5() {
    String value = "1978/04/aa";
    LocalDate localDate = DateUtils.parseToLocalDate(value);
  }

  @Test(expected = DateTimeParseException.class)
  public void testParseToLocalDate6() {
    String value = "1978/04";
    LocalDate localDate = DateUtils.parseToLocalDate(value);
  }

  @Test
  public void testLocalDateToString1() {
    String result = DateUtils.toString(LocalDate.of(2017, 5, 14));
    Assert.assertEquals("2017/05/14", result);
  }

  @Test
  public void testLocalDateToString2() {
    Assert.assertNull(DateUtils.toString(null));
  }

}
