package com.nannieshare.beans.user;

import com.nannieshare.enums.user.MediaType;
import org.junit.Assert;
import org.junit.Test;

public class MediaUtilsTest {

  @Test
  public void testCreateMediaNameFromFileName() {
    MediaBean bean = new MediaBean();
    bean.setName("test1.jpg");
    bean.setType(MediaType.PICTURE);

    String result = MediaUtils.createMediaName(1, bean, 4444);
    Assert.assertNotNull(result);
    Assert.assertEquals("1_picture_115c.jpg", result);
  }

  @Test
  public void testCreateMediaNameFromContentType() {
    MediaBean bean = new MediaBean();
    bean.setName("this_is_test2");
    bean.setContentType("image/png");
    bean.setType(MediaType.PICTURE);

    String result = MediaUtils.createMediaName(1, bean, 4444);
    Assert.assertNotNull(result);
    Assert.assertEquals("1_picture_115c.png", result);
  }

  @Test
  public void testCreateMediaNamePrecendence() {
    MediaBean bean = new MediaBean();
    bean.setName("name.jpg");
    bean.setContentType("image/png");
    bean.setType(MediaType.PICTURE);

    String result = MediaUtils.createMediaName(2, bean, 4444);
    Assert.assertNotNull(result);
    Assert.assertEquals("2_picture_115c.png", result);
  }

  @Test
  public void testMissingNameAndContentType() {
    MediaBean bean = new MediaBean();
    bean.setName("name");
    bean.setType(MediaType.PICTURE);

    String result = MediaUtils.createMediaName(3, bean, 4444);
    Assert.assertNotNull(result);
    Assert.assertEquals("3_picture_115c", result);
  }

  @Test
  public void testMissingMediaType() {
    MediaBean bean = new MediaBean();
    bean.setName("name");

    String result = MediaUtils.createMediaName(4, bean, 4444);
    Assert.assertNotNull(result);
    Assert.assertEquals("4_unknown_115c", result);
  }

}