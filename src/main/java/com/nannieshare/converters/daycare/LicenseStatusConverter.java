package com.nannieshare.converters.daycare;

import com.nannieshare.enums.daycare.LicenseStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LicenseStatusConverter implements AttributeConverter<LicenseStatus, Integer> {

  @Override
  public Integer convertToDatabaseColumn(LicenseStatus type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public LicenseStatus convertToEntityAttribute(Integer value) {
    return value == null ? null : LicenseStatus.from(value);
  }
}