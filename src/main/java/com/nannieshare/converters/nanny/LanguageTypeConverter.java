package com.nannieshare.converters.nanny;

import com.nannieshare.enums.nanny.LanguageType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LanguageTypeConverter implements AttributeConverter<LanguageType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(LanguageType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public LanguageType convertToEntityAttribute(Integer value) {
    return value == null ? null : LanguageType.from(value);
  }
}