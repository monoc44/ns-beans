package com.nannieshare.converters.nanny;

import com.nannieshare.enums.nanny.LanguageLevelType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LanguageLevelConverter implements AttributeConverter<LanguageLevelType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(LanguageLevelType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public LanguageLevelType convertToEntityAttribute(Integer value) {
    return value == null ? null : LanguageLevelType.from(value);
  }
}