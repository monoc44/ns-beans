package com.nannieshare.converters;


import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

public final class HtmlHelper {

  private static final PolicyFactory SANITIZER = new HtmlPolicyBuilder()
    .allowElements("a", "div", "b", "i", "strike", "u", "sup", "sub", "ul", "ol", "li", "blockquote", "br")
    .allowAttributes("href").onElements("a")
    .allowAttributes("style").onElements("blockquote")
    .allowAttributes("style").onElements("div")
    .allowAttributes("style").onElements("i")
    .allowUrlProtocols("https")
    .requireRelNofollowOnLinks()
    .toFactory();

  public static String sanitize(String untrustedHTML) {
    if (untrustedHTML == null || untrustedHTML.trim().isEmpty()) {
      return null;
    }
    return SANITIZER.sanitize(untrustedHTML);
  }

}
