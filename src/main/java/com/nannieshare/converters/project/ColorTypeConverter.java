package com.nannieshare.converters.project;

import com.nannieshare.enums.project.ColorType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ColorTypeConverter implements AttributeConverter<ColorType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(ColorType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public ColorType convertToEntityAttribute(Integer value) {
    return value == null ? null : ColorType.from(value);
  }
}