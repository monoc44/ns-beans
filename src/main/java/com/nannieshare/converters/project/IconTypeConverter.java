package com.nannieshare.converters.project;

import com.nannieshare.enums.project.IconType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class IconTypeConverter implements AttributeConverter<IconType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(IconType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public IconType convertToEntityAttribute(Integer value) {
    return value == null ? null : IconType.from(value);
  }
}