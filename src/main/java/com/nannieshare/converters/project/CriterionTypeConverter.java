package com.nannieshare.converters.project;

import com.nannieshare.enums.project.CriterionType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CriterionTypeConverter implements AttributeConverter<CriterionType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(CriterionType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public CriterionType convertToEntityAttribute(Integer value) {
    return value == null ? null : CriterionType.from(value);
  }
}