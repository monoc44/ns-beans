package com.nannieshare.converters.user;

import com.nannieshare.enums.user.DayType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DayTypeConverter implements AttributeConverter<DayType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(DayType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public DayType convertToEntityAttribute(Integer value) {
    return value == null ? null : DayType.from(value);
  }
}