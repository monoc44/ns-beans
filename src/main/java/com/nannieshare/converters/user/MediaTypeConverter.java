package com.nannieshare.converters.user;

import com.nannieshare.enums.user.MediaType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MediaTypeConverter implements AttributeConverter<MediaType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(MediaType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public MediaType convertToEntityAttribute(Integer value) {
    return value == null ? null : MediaType.from(value);
  }
}