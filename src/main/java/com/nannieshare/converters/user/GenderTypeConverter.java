package com.nannieshare.converters.user;

import com.nannieshare.enums.user.GenderType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class GenderTypeConverter implements AttributeConverter<GenderType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(GenderType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public GenderType convertToEntityAttribute(Integer value) {
    return value == null ? null : GenderType.from(value);
  }
}