package com.nannieshare.converters.user;

import com.nannieshare.enums.user.RoleType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RoleTypeConverter implements AttributeConverter<RoleType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(RoleType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public RoleType convertToEntityAttribute(Integer value) {
    return value == null ? null : RoleType.from(value);
  }
}