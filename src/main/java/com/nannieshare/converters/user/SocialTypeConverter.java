package com.nannieshare.converters.user;

import com.nannieshare.enums.user.SocialType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SocialTypeConverter implements AttributeConverter<SocialType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(SocialType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public SocialType convertToEntityAttribute(Integer value) {
    return value == null ? null : SocialType.from(value);
  }
}