package com.nannieshare.converters.user;

import com.nannieshare.enums.user.UserType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class UserTypeConverter implements AttributeConverter<UserType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(UserType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public UserType convertToEntityAttribute(Integer value) {
    return value == null ? null : UserType.from(value);
  }
}