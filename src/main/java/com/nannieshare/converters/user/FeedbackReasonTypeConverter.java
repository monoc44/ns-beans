package com.nannieshare.converters.user;

import com.nannieshare.enums.user.FeedbackReasonType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class FeedbackReasonTypeConverter implements AttributeConverter<FeedbackReasonType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(FeedbackReasonType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public FeedbackReasonType convertToEntityAttribute(Integer value) {
    return value == null ? null : FeedbackReasonType.from(value);
  }
}