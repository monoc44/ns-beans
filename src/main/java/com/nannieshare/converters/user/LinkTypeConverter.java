package com.nannieshare.converters.user;

import com.nannieshare.enums.user.LinkType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LinkTypeConverter implements AttributeConverter<LinkType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(LinkType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public LinkType convertToEntityAttribute(Integer value) {
    return value == null ? null : LinkType.from(value);
  }
}