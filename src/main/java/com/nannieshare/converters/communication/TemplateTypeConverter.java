package com.nannieshare.converters.communication;

import com.nannieshare.enums.communication.TemplateType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TemplateTypeConverter implements AttributeConverter<TemplateType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(TemplateType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public TemplateType convertToEntityAttribute(Integer value) {
    return value == null ? null : TemplateType.from(value);
  }
}