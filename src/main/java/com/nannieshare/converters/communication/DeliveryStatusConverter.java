package com.nannieshare.converters.communication;

import com.nannieshare.enums.communication.DeliveryStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DeliveryStatusConverter implements AttributeConverter<DeliveryStatus, Integer> {

  @Override
  public Integer convertToDatabaseColumn(DeliveryStatus type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public DeliveryStatus convertToEntityAttribute(Integer value) {
    return value == null ? null : DeliveryStatus.from(value);
  }
}