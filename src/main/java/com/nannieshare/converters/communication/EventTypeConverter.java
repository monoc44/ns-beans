package com.nannieshare.converters.communication;

import com.nannieshare.enums.communication.EventType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EventTypeConverter implements AttributeConverter<EventType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(EventType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public EventType convertToEntityAttribute(Integer value) {
    return value == null ? null : EventType.from(value);
  }
}