package com.nannieshare.converters.communication;

import com.nannieshare.enums.communication.NotificationStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class NotificationStatusConverter implements AttributeConverter<NotificationStatus, Integer> {

  @Override
  public Integer convertToDatabaseColumn(NotificationStatus type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public NotificationStatus convertToEntityAttribute(Integer value) {
    return value == null ? null : NotificationStatus.from(value);
  }
}