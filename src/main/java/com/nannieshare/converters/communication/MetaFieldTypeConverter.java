package com.nannieshare.converters.communication;

import com.nannieshare.enums.communication.MetaFieldType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MetaFieldTypeConverter implements AttributeConverter<MetaFieldType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(MetaFieldType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public MetaFieldType convertToEntityAttribute(Integer value) {
    return value == null ? null : MetaFieldType.from(value);
  }
}