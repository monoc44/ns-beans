package com.nannieshare.converters.billing;

import com.nannieshare.enums.billing.SourceStatusType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SourceStatusTypeConverter implements AttributeConverter<SourceStatusType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(SourceStatusType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public SourceStatusType convertToEntityAttribute(Integer value) {
    return value == null ? null : SourceStatusType.from(value);
  }
}