package com.nannieshare.converters.billing;

import com.nannieshare.enums.billing.CardBrandType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CardBrandTypeConverter implements AttributeConverter<CardBrandType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(CardBrandType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public CardBrandType convertToEntityAttribute(Integer value) {
    return value == null ? null : CardBrandType.from(value);
  }
}