package com.nannieshare.converters.billing;

import com.nannieshare.enums.billing.SubscriptionStatus;

import javax.persistence.AttributeConverter;

public class SubscriptionStatusConverter  implements AttributeConverter<SubscriptionStatus, Integer> {

  @Override
  public Integer convertToDatabaseColumn(SubscriptionStatus type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public SubscriptionStatus convertToEntityAttribute(Integer value) {
    return value == null ? null : SubscriptionStatus.from(value);
  }
}