package com.nannieshare.converters.billing;

import com.nannieshare.enums.billing.CardFundingType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CardFundingTypeConverter implements AttributeConverter<CardFundingType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(CardFundingType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public CardFundingType convertToEntityAttribute(Integer value) {
    return value == null ? null : CardFundingType.from(value);
  }
}