package com.nannieshare.converters.map;

import com.nannieshare.enums.map.ChildCareType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ChildCareTypeConverter implements AttributeConverter<ChildCareType, Integer> {

  @Override
  public Integer convertToDatabaseColumn(ChildCareType type) {
    return type == null ? null : Integer.valueOf(type.id());
  }

  @Override
  public ChildCareType convertToEntityAttribute(Integer value) {
    return value == null ? null : ChildCareType.from(value);
  }
}