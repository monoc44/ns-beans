package com.nannieshare.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.Instant;

@Converter
public class InstantConverter implements AttributeConverter<Instant, Timestamp> {
	
    @Override
    public Timestamp convertToDatabaseColumn(Instant locDate) {
    	return locDate == null ? null : Timestamp.from(locDate);
    }

    @Override
    public Instant convertToEntityAttribute(Timestamp timestamp) {
    	Instant instant = timestamp == null ? null : timestamp.toInstant();
//      System.out.println("Converting " + timestamp + " to instant:" + instant);
      return instant;
    }
}