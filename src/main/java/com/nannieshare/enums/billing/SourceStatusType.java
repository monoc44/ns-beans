package com.nannieshare.enums.billing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum SourceStatusType {
  CANCELED(0),
  CHARGEABLE(1),
  CONSUMED(2),
  FAILED(3),
  PENDING(4);

  private static final Logger LOG = LoggerFactory.getLogger(SourceStatusType.class);

  private int id;

  SourceStatusType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static SourceStatusType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static SourceStatusType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (SourceStatusType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(SourceStatusType.class, value);
  }

  public static SourceStatusType from(Integer id) {
    if (id != null) {
      for (SourceStatusType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(SourceStatusType.class, id);
  }

  public int id() {
    return id;
  }
}

