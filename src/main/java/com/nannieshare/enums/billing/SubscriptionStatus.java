package com.nannieshare.enums.billing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum SubscriptionStatus {
  TRIALING(0),
  ACTIVE(1),
  PAST_DUE(2),
  CANCELED(3),
  UNPAID(4);

  private int id;

  SubscriptionStatus(int id) {
    this.id = id;
  }

  @JsonCreator
  public static SubscriptionStatus forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static SubscriptionStatus from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (SubscriptionStatus type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(SubscriptionStatus.class, value);
  }

  public static SubscriptionStatus from(Integer id) {
    if (id != null) {
      for (SubscriptionStatus type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(SubscriptionStatus.class, id);
  }

  public int id() {
    return id;
  }
}

