package com.nannieshare.enums.billing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum CardFundingType {
  CREDIT(0),
  DEBIT(1),
  PREPAID(2),
  UNKNOWN(3);

  private int id;

  CardFundingType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static CardFundingType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static CardFundingType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (CardFundingType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(CardFundingType.class, value);
  }

  public static CardFundingType from(Integer id) {
    if (id != null) {
      for (CardFundingType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(CardFundingType.class, id);
  }

  public int id() {
    return id;
  }
}

