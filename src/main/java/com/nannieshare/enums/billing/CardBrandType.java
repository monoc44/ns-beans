package com.nannieshare.enums.billing;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum CardBrandType {

  AMERICAN_EXPRESS(0),
  DINERS_CLUB(1),
  DISCOVER(2),
  JCB(3),
  MASTERCARD(4),
  UNIONPAY(5),
  VISA(6),
  UNKNOWN(7);

  private int id;

  CardBrandType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static CardBrandType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static CardBrandType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (CardBrandType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(CardBrandType.class, value);
  }

  public static CardBrandType from(Integer id) {
    if (id != null) {
      for (CardBrandType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(CardBrandType.class, id);
  }

  public int id() {
    return id;
  }
}

