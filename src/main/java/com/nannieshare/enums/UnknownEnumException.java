package com.nannieshare.enums;

public class UnknownEnumException extends RuntimeException {

  public <T> UnknownEnumException(Class<T> clazz, Integer id) {
    super(String.format("Could not find an entry in enum %s with id: '%s'", clazz.getSimpleName(), id));
  }

  public <T> UnknownEnumException(Class<T> clazz, String name) {
    super(String.format("Could not find an entry in enum %s with name: '%s'", clazz.getSimpleName(), name));
  }
}
