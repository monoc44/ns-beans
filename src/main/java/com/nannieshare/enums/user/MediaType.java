package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum MediaType {

  PICTURE(1),
  VIDEO(2);

  private int id;

  MediaType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static MediaType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static MediaType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (MediaType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(MediaType.class, value);
  }

  public static MediaType from(Integer id) {
    if (id != null) {
      for (MediaType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(MediaType.class, id);
  }

  public int id() {
    return id;
  }
}

