package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum RoleType {

  BASIC(1),
  ADMIN(2),
  PREMIUM(3);

  private int id;

  RoleType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static RoleType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static RoleType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (RoleType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(RoleType.class, value);
  }

  public static RoleType from(Integer id) {
    if (id != null) {
      for (RoleType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(RoleType.class, id);
  }

  public int id() {
    return id;
  }
}

