package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum FeedbackReasonType {

  NO_NEED_NANNYSHARE(0),
  FOUND_NANNYSHARE(1),
  NOT_PLEASED(2),
  TOO_EXPENSIVE(3),
  OTHER(4);

  private int id;

  FeedbackReasonType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static FeedbackReasonType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static FeedbackReasonType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (FeedbackReasonType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(FeedbackReasonType.class, value);
  }

  public static FeedbackReasonType from(Integer id) {
    if (id != null) {
      for (FeedbackReasonType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(FeedbackReasonType.class, id);
  }

  public int id() {
    return id;
  }

}
