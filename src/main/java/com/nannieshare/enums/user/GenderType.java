package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum GenderType {

  MALE(0),
  FEMALE(1),
  UNKNOWN(2);

  private int id;

  GenderType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static GenderType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static GenderType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (GenderType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(GenderType.class, value);
  }

  public static GenderType from(Integer id) {
    if (id != null) {
      for (GenderType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(GenderType.class, id);
  }

  public int id() {
    return id;
  }
}

