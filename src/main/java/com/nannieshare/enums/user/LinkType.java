package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum LinkType {

  VISITED(0),
  FAVORITE(1),
  X_OUT(2),
  SUGGESTED(3),
  PINGED(4);

  private int id;

  LinkType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static LinkType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static LinkType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (LinkType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LinkType.class, value);
  }

  public static LinkType from(Integer id) {
    if (id != null) {
      for (LinkType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LinkType.class, id);
  }

  public int id() {
    return id;
  }
}

