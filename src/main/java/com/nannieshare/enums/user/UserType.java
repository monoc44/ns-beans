package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum UserType {

  PARENT(0),
  NANNY(1),
  DAYCARE(2),
  ALL(99);

  private int id;

  UserType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static UserType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static UserType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (UserType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(UserType.class, value);
  }

  public static UserType from(Integer id) {
    if (id != null) {
      for (UserType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(UserType.class, id);
  }

  public int id() {
    return id;
  }

  public boolean isParent() {
    return this.id == UserType.PARENT.id;
  }

  public boolean isNanny() {
    return this.id == UserType.NANNY.id;
  }

  public boolean isDaycare() {
    return this.id == UserType.DAYCARE.id;
  }

}
