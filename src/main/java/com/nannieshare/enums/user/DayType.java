package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;
import com.nannieshare.enums.project.CriterionType;

public enum DayType {
  MON(0),
  TUE(1),
  WED(2),
  THU(3),
  FRI(4),
  SAT(5),
  SUN(6);

  private int id;

  DayType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static DayType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static DayType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (DayType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(DayType.class, value);
  }

  public static DayType from(Integer id) {
    if (id != null) {
      for (DayType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(DayType.class, id);
  }

  public int id() {
    return id;
  }

  public CriterionType[] asCriteria() {
    CriterionType[] toReturn = new CriterionType[4];
    toReturn[0] = CriterionType.from((id() * 4));
    toReturn[1] = CriterionType.from((id() * 4) + 1);
    toReturn[2] = CriterionType.from((id() * 4) + 2);
    toReturn[3] = CriterionType.from((id() * 4) + 3);
    return toReturn;
  }

}

