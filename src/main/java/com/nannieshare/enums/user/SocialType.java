package com.nannieshare.enums.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum SocialType {

  FACEBOOK(0),
  GOOGLE(1);

  private int id;

  SocialType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static SocialType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static SocialType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (SocialType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(SocialType.class, value);
  }

  public static SocialType from(Integer id) {
    if (id != null) {
      for (SocialType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(SocialType.class, id);
  }

  public int id() {
    return id;
  }}
