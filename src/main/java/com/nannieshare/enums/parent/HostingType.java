package com.nannieshare.enums.parent;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

/**
 * No converter for DB as this type is only used between UI and API
 */
public enum HostingType {

  THEIR_HOME(0),
  NO_PREFERENCES(1),
  MY_HOME(2);

  private int id;

  HostingType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static HostingType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static HostingType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (HostingType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(HostingType.class, value);
  }

  public static HostingType from(Integer id) {
    if (id != null) {
      for (HostingType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(HostingType.class, id);
  }

  public int id() {
    return id;
  }

}