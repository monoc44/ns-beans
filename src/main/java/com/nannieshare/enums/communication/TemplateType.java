package com.nannieshare.enums.communication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum TemplateType {

  ACTIVATION(0, "templates/notifications/activation/v%d.%s", (short) 2),
  LOST_PASSWORD(1, "templates/notifications/lostPassword/v%d.%s", (short) 2),
  NANNY_INVITE(2, "templates/notifications/nannyInvite/v%d.%s", (short) 1),
  EVENT_NEW_FAMILY(3, "templates/notifications/newFamily/v%d.%s", (short) 1),
  EVENT_NEW_NANNY(4, "templates/notifications/newFamily/v%d.%s", (short) 1),
  EVENT_NEW_MESSAGE(5, "templates/notifications/newMessage/v%d.%s", (short) 1),
  EVENT_SUGGESTIONS(6, "templates/notifications/suggestions/v%d.%s", (short) 1),
  CHECK_ACTIVE_STATUS(7, "templates/notifications/checkActive/v%d.%s", (short) 1),
  ANNOUNCEMENT(99, "templates/announcements/%s/v%d.%s", (short) 1);

  private int id;
  private short latestVersion;
  private String fileName;

  TemplateType(int value, String fileName, short latestVersion) {
    this.id = value;
    this.latestVersion = latestVersion;
    this.fileName = fileName;
  }

  @JsonCreator
  public static TemplateType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static TemplateType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (TemplateType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(TemplateType.class, value);
  }

  public static TemplateType from(Integer id) {
    if (id != null) {
      for (TemplateType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(TemplateType.class, id);
  }

  public int id() {
    return id;
  }

  public short getLatestVersion() {
    return latestVersion;
  }

  public String getFileName() {
    return fileName;
  }
}
