package com.nannieshare.enums.communication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum MetaFieldType {

  // NOTIFIED USER FIELDS
  USER_ID(0),
  USER_TYPE(1),
  FIRST_NAME(2),
  LAST_NAME(3),
  EMAIL(4),
  PHONE_NUMBER(5),
  ADDRESS(6),
  ADDRESS_LAT(7),
  ADDRESS_LNG(8),
  PROJECT_ID(9),

  // OTHER RELATED USER FIELDS
  OTHER_USER_ID(30),
  OTHER_USER_TYPE(31),
  OTHER_FIRST_NAME(32),
  OTHER_LAST_NAME(33),
  OTHER_EMAIL(34),
  OTHER_PHONE_NUMBER(35),
  OTHER_ADDRESS(36),
  OTHER_ADDRESS_LAT(37),
  OTHER_ADDRESS_LNG(38),
  OTHER_PROJECT_ID(39),

  // POSSIBLE LINKS
  LINK1(60),
  LINK2(61),
  LINK3(62),
  LINK4(63),

  // META OR MISC. INFORMATION
  TOKEN(90),
  MESSAGE(91),
  DESCRIPTION(92),
  DISTANCE(93),
  FROM(94),
  TO(95);

  private int id;

  MetaFieldType(int value) {
    this.id = value;
  }

  @JsonCreator
  public static MetaFieldType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static MetaFieldType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (MetaFieldType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(MetaFieldType.class, value);
  }

  public static MetaFieldType from(Integer id) {
    if (id != null) {
      for (MetaFieldType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(MetaFieldType.class, id);
  }

  public int id() {
    return id;
  }

}
