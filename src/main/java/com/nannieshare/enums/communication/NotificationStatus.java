package com.nannieshare.enums.communication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum NotificationStatus {

  PENDING(0),
  SKIPPED(1),
  UNREAD(2),
  READ(3);

  private int id;

  NotificationStatus(int value) {
    this.id = value;
  }

  @JsonCreator
  public static NotificationStatus forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static NotificationStatus from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (NotificationStatus type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(NotificationStatus.class, value);
  }

  public static NotificationStatus from(Integer id) {
    if (id != null) {
      for (NotificationStatus type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(NotificationStatus.class, id);
  }

  public int id() {
    return id;
  }

}
