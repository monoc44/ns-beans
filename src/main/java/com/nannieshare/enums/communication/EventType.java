package com.nannieshare.enums.communication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum EventType {

  EMAIL_ACTIVATION(0),
  EMAIL_RECOVER_LOST_PASSWORD(1),
  NANNY_INVITE(2),
  NEW_FAMILY_NEARBY(3),
  NEW_NANNY_NEARBY(4),
  NEW_MESSAGE(5),
  TEAM_UP_REQUEST(6),
  MARKED_AS_FAVORITE(7),
  PROFILE_VIEWED(8),
  FAMILIES_AROUND(9),
  CHECK_ACTIVE_STATUS(10),
  ANNOUNCEMENT(99);

  private int id;

  EventType(int value) {
    this.id = value;
  }

  @JsonCreator
  public static EventType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static EventType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (EventType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(EventType.class, value);
  }

  public static EventType from(Integer id) {
    if (id != null) {
      for (EventType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(EventType.class, id);
  }

  public int id() {
    return id;
  }

}
