package com.nannieshare.enums.communication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum DeliveryStatus {

  PENDING(0),
  SUCCESS(1),
  FAILED(2);

  private int id;

  DeliveryStatus(int value) {
    this.id = value;
  }

  @JsonCreator
  public static DeliveryStatus forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static DeliveryStatus from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (DeliveryStatus type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(DeliveryStatus.class, value);
  }

  public static DeliveryStatus from(Integer id) {
    if (id != null) {
      for (DeliveryStatus type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(DeliveryStatus.class, id);
  }

  public int id() {
    return id;
  }
}
