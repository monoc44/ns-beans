package com.nannieshare.enums.nanny;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum LanguageLevelType {

  BASIC(0),
  FLUENT(1),
  NATIVE_SPEAKER(2);

  private int id;

  LanguageLevelType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static LanguageLevelType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static LanguageLevelType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (LanguageLevelType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LanguageLevelType.class, value);
  }

  public static LanguageLevelType from(Integer id) {
    if (id != null) {
      for (LanguageLevelType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LanguageLevelType.class, id);
  }

  public int id() {
    return id;
  }

}