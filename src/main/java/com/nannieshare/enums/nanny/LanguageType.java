package com.nannieshare.enums.nanny;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum LanguageType {

  ENGLISH(0),
  SPANISH(1),
  CHINESE(2),
  HINDI(3),
  ARABIC(4),
  PORTUGUESE(5),
  BENGALI(6),
  RUSSIAN(7),
  JAPANESE(8),
  LAHNDA(9),
  JAVANESE(10),
  GERMAN(11),
  KOREAN(12),
  FRENCH(13),
  TELUGU(14),
  MARATHI(15),
  TURKISH(16),
  TAMIL(17),
  VIETNAMESE(18),
  URDU(19),
  ITALIAN(20),
  POLISH(21),
  DUTCH(22),
  DANISH(23),
  CROATIAN(24),
  CZECH(25),
  ESTONIAN(26),
  FINNISH(27),
  GREEK(28),
  HUNGARIAN(29),
  IRISH(30),
  LATVIAN(31),
  LITHUANIAN(32),
  MALTESE(33),
  ROMANIAN(34),
  SLOVAK(35),
  SLOVENIAN(36),
  SWEDISH(37);
  private int id;

  LanguageType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static LanguageType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static LanguageType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (LanguageType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LanguageType.class, value);
  }

  public static LanguageType from(Integer id) {
    if (id != null) {
      for (LanguageType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LanguageType.class, id);
  }

  public int id() {
    return id;
  }

  public static Map<Integer, String> asProperties() {
    return Arrays.stream(LanguageType.values()).collect(Collectors.toMap(t -> Integer.valueOf(t.id()), t -> StringUtils.capitalize(t.name())));
  }

}