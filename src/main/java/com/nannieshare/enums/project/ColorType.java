package com.nannieshare.enums.project;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum ColorType {
  BLUE(0),
  GREEN(1),
  RED(2),
  PURPLE(3),
  YELLOW(4);

  private int id;

  ColorType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static ColorType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static ColorType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (ColorType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(ColorType.class, value);
  }

  public static ColorType from(Integer id) {
    if (id != null) {
      for (ColorType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(ColorType.class, id);
  }

  public int id() {
    return id;
  }
}

