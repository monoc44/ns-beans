package com.nannieshare.enums.project;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum IconType {
  AGENDA(0),
  COFFEE(1),
  BEACH(2);

  private int id;

  IconType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static IconType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static IconType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (IconType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(IconType.class, value);
  }

  public static IconType from(Integer id) {
    if (id != null) {
      for (IconType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(IconType.class, id);
  }

  public int id() {
    return id;
  }
}

