package com.nannieshare.enums.project;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum CriterionType {

  MONDAY_START_A(0),
  MONDAY_END_A(1),
  MONDAY_START_B(2),
  MONDAY_END_B(3),
  TUESDAY_START_A(4),
  TUESDAY_END_A(5),
  TUESDAY_START_B(6),
  TUESDAY_END_B(7),
  WEDNESDAY_START_A(8),
  WEDNESDAY_END_A(9),
  WEDNESDAY_START_B(10),
  WEDNESDAY_END_B(11),
  THURSDAY_START_A(12),
  THURSDAY_END_A(13),
  THURSDAY_START_B(14),
  THURSDAY_END_B(15),
  FRIDAY_START_A(16),
  FRIDAY_END_A(17),
  FRIDAY_START_B(18),
  FRIDAY_END_B(19),
  SATURDAY_START_A(20),
  SATURDAY_END_A(21),
  SATURDAY_START_B(22),
  SATURDAY_END_B(23),
  SUNDAY_START_A(24),
  SUNDAY_END_A(25),
  SUNDAY_START_B(26),
  SUNDAY_END_B(27),
  MILE_RADIUS(28),
  MAX_NB_CHILDREN(29),
  PETS_OKAY(30),
  USE_CAR_OKAY(31),
  WALK_OUTSIDE(32),
  RATE_ONE_CHILD(33),
  RATE_TWO_CHILDREN(34),
  RATE_THREE_CHILDREN(35),
  RATE_FOUR_CHILDREN(36),
  RATE_EXTENDED_CARE_PER_CHILD(37),
  EXTENDED_CARE_AT_NANNY_HOME(38),
  CHILD_ABSENCE_FULLY_PAID(39),
  CHILD_SICK_FULLY_PAID(40),
  CHILD_ON_VACATION_FULLY_PAID(41),
  TAXES(42),
  NANNY_VACATION_NB_DAYS(43),
  NANNY_SICK_LEAVE_NB_DAYS(44),
  NANNY_HOLIDAYS_NB_DAYS(45),
  IS_ACTIVE(46),
  SEEK_NANNY(47),
  SEEK_PARENTS(48),
  CAN_HOST(49),
  SPOKEN_LANGUAGE(50),
  SPOKEN_LANGUAGE_BONUS(51),
  USE_NS_PAYMENT(52);

  // Type of criteria
  private int id;

  CriterionType(int id) {
    this.id = id;
  }

  @JsonCreator
  public static CriterionType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static CriterionType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (CriterionType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(CriterionType.class, value);
  }

  public static CriterionType from(Integer id) {
    if (id != null) {
      for (CriterionType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(CriterionType.class, id);
  }

  public int id() {
    return id;
  }

  public static List<CriterionType> getAgendaCriteria() {
    return Arrays.stream(values())
      .filter(c -> c.id >= MONDAY_START_A.id() && c.id <= SUNDAY_END_B.id())
      .collect(Collectors.toList());
  }

  public static List<CriterionType> getWeekdayAgendaCriteria() {
    return Arrays.stream(values())
      .filter(c -> c.id >= MONDAY_START_A.id() && c.id <= FRIDAY_END_B.id())
      .collect(Collectors.toList());
  }

}
