package com.nannieshare.enums.daycare;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum LicenseStatus {

  LICENSED(0),
  PENDING(1),
  CLOSED(2),
  INACTIVE(3),
  ON_PROBATION(4);

  private int id;

  LicenseStatus(int value) {
    this.id = value;
  }

  @JsonCreator
  public static LicenseStatus forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static LicenseStatus from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (LicenseStatus type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LicenseStatus.class, value);
  }

  public static LicenseStatus from(Integer id) {
    if (id != null) {
      for (LicenseStatus type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(LicenseStatus.class, id);
  }

  public int id() {
    return id;
  }
}
