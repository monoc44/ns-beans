package com.nannieshare.enums.map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.nannieshare.StringUtils;
import com.nannieshare.enums.UnknownEnumException;

public enum ChildCareType {

  DAY_CARE_CENTER(0, "dcc"),
  DAY_CARE_CENTER_ILL_CENTER(1, "dccic"),
  INFANT_CENTER(2, "ic"),
  SCHOOL_AGE_DAY_CARE_CENTER(3, "sadcc"),
  FAMILY_DAY_CARE_HOME(4, "fdch"),
  PARENT_SEEKER(5, "par"),
  PARENT_NS(6, "par-ns"),
  NANNY(7, "nan");

  private int id;
  private String shortName;

  ChildCareType(int id, String shortName) {
    this.id = id;
    this.shortName = shortName;
  }

  @JsonCreator
  public static ChildCareType forValue(String value) {
    return from(value);
  }

  @JsonValue
  final int value() {
    return id;
  }

  public static ChildCareType from(String value) {
    if (StringUtils.isInteger(value)) {
      return from(Integer.parseInt(value));
    }
    if (value != null) {
      String formattedValue = value.toUpperCase().replaceAll(" ", "_");
      for (ChildCareType type : values()) {
        if (type.name().equalsIgnoreCase(formattedValue)) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(ChildCareType.class, value);
  }

  public static ChildCareType from(Integer id) {
    if (id != null) {
      for (ChildCareType type : values()) {
        if (type.id == id.intValue()) {
          return type;
        }
      }
    }
    throw new UnknownEnumException(ChildCareType.class, id);
  }

  public int id() {
    return id;
  }

  public String shortName() {
    return shortName;
  }
}
