package com.nannieshare;

import com.google.common.net.UrlEscapers;
import com.nannieshare.enums.communication.MetaFieldType;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class StringUtils {

  private static final Pattern CAPITALIZE_BOUNDER = Pattern.compile("\\b(?=\\w)");

  public static final String capitalize(String value) {
    if (value == null) return null;
    return CAPITALIZE_BOUNDER
      .splitAsStream(value.toLowerCase())
      .map(StringUtils::upperFirstLetter)
      .collect(Collectors.joining());
  }

  public static String upperFirstLetter(String input) {
    if (input == null || input.length() < 2) {
      return input;
    }
    return input.substring(0, 1).toUpperCase() + input.substring(1);
  }

  /**
   * @param strings the strings to evaluate
   * @return true if one of the items is empty or null
   */
  public static boolean containsBlank(String... strings) {
    if (strings == null || strings.length == 0) {
      return true;
    }
    for (String string : strings) {
      if (string == null || string.trim().isEmpty()) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param strings the strings to evaluate
   * @return true if one of the items contains something, i.e. not null nor empty
   */
  public static boolean containsNoBlank(String... strings) {
    if (strings == null || strings.length == 0) {
      return false;
    }
    for (String string : strings) {
      if (string != null && !string.trim().isEmpty()) {
        return true;
      }
    }
    return false;
  }

  public static String extractAfterLast(String string, String pattern) {
    if (containsBlank(string)) {
      return null;
    }
    int lastIndex = string.lastIndexOf(pattern);
    return (lastIndex != -1 && lastIndex < string.length() - 1) ? string.substring(lastIndex + 1) : null;
  }

  public static String encodeBase64(String string) throws UnsupportedEncodingException {
    byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
    return Base64.getUrlEncoder().encodeToString(bytes);
  }

  public static String decodeBase64(String string) {
    byte[] decoded = Base64.getUrlDecoder().decode(string);
    return new String(decoded);
  }

  public static boolean isInteger(String value) {
    return isInteger(value, 10);
  }

  public static boolean isInteger(String value, int radix) {
    if (value == null || value.trim().isEmpty()) return false;
    Scanner sc = new Scanner(value.trim());
    if(!sc.hasNextInt(radix)) return false;
    // we know it starts with a valid int, now make sure there's nothing left!
    sc.nextInt(radix);
    return !sc.hasNext();
  }

  public static String encodeUrlParameter(String value) {
    if (value == null) return "";
    String toReturn = UrlEscapers.urlFragmentEscaper().escape(value);
    // Noticed that some special chars remain unescaped, so let's do something here
    toReturn = toReturn.replaceAll("\\+", "%2B");
    toReturn = toReturn.replaceAll("\\/", "%2F");
    toReturn = toReturn.replaceAll("\\=", "%3D");
    return toReturn;
  }
}

