package com.nannieshare;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateUtils {

  public static final DateTimeFormatter DATE_PARSER = DateTimeFormatter.ofPattern("yyyy/MM/dd");
  public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");

  public static LocalDate parseToLocalDate(String date) {
    if (date == null || date.trim().isEmpty()) return null;
    if (date.length() < 10) {
      throw new DateTimeParseException("Invalid local date received: " + date, date, 10);
    }
    if (date.length() > 10) {
      date = date.substring(0, 10);
    }
    return LocalDate.parse(date, DATE_PARSER);
  }

  public static String toString(LocalDate date) {
    if (date == null) return null;
    return date.format(DATE_FORMATTER);
  }

}
