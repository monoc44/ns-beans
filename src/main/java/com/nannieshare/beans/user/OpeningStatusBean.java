package com.nannieshare.beans.user;

public class OpeningStatusBean {

  private boolean open;
  private Integer etaInWeeks;

  /**
   * GETTERS AND SETTERS
   */

  public boolean isOpen() {
    return open;
  }

  public void setOpen(boolean open) {
    this.open = open;
  }

  public Integer getEtaInWeeks() {
    return etaInWeeks;
  }

  public void setEtaInWeeks(Integer etaInWeeks) {
    this.etaInWeeks = etaInWeeks;
  }
}
