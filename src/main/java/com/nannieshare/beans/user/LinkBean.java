package com.nannieshare.beans.user;

import com.nannieshare.enums.user.LinkType;
import com.nannieshare.enums.user.UserType;

public class LinkBean {

  private Integer fromUserId;
  private UserType fromUserType;

  private Integer toUserId;
  private UserType toUserType;

  private LinkType type;

  /**
   * GETTERS AND SETTERS
   */

  public Integer getFromUserId() {
    return fromUserId;
  }

  public void setFromUserId(Integer fromUserId) {
    this.fromUserId = fromUserId;
  }

  public UserType getFromUserType() {
    return fromUserType;
  }

  public void setFromUserType(UserType fromUserType) {
    this.fromUserType = fromUserType;
  }

  public Integer getToUserId() {
    return toUserId;
  }

  public void setToUserId(Integer toUserId) {
    this.toUserId = toUserId;
  }

  public UserType getToUserType() {
    return toUserType;
  }

  public void setToUserType(UserType toUserType) {
    this.toUserType = toUserType;
  }

  public LinkType getType() {
    return type;
  }

  public void setType(LinkType type) {
    this.type = type;
  }
}
