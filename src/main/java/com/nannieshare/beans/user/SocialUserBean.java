package com.nannieshare.beans.user;

import javax.validation.constraints.NotBlank;

public class SocialUserBean {

  @NotBlank
  private String accessToken;

  private String socialId;
  private Integer expiresIn;
  private String signedRequest;
  private String scope;
  private String tokenType;
  private String nannyToken;

  /**
   * GETTERS AND SETTERS
   */

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getSocialId() {
    return socialId;
  }

  public void setSocialId(String socialId) {
    this.socialId = socialId;
  }

  public Integer getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(Integer expiresIn) {
    this.expiresIn = expiresIn;
  }

  public String getSignedRequest() {
    return signedRequest;
  }

  public void setSignedRequest(String signedRequest) {
    this.signedRequest = signedRequest;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public String getNannyToken() {
    return nannyToken;
  }

  public void setNannyToken(String nannyToken) {
    this.nannyToken = nannyToken;
  }
}
