package com.nannieshare.beans.user;

import com.nannieshare.StringUtils;

import java.util.Random;

public final class MediaUtils {

  private static final Random random = new Random();

  public static String createMediaName(Integer userId, MediaBean bean) {
    return createMediaName(userId, bean, random.nextInt());
  }

  public static String createMediaName(Integer userId, MediaBean bean, int pictureId) {
    String pictureName = "%d_%s_%x%s";
    String type = bean.getType() != null ? bean.getType().name().toLowerCase() : "unknown";
    String extension = findMediaExtension(bean);
    return String.format(pictureName, userId, type, pictureId, extension);
  }


  public static String findMediaExtension(MediaBean bean) {
    String toReturn = StringUtils.extractAfterLast(bean.getContentType(), "/");
    if (StringUtils.containsBlank(toReturn)) {
      toReturn = StringUtils.extractAfterLast(bean.getName(), ".");
    }
    return StringUtils.containsBlank(toReturn) ? "" : "." + toReturn;
  }

}
