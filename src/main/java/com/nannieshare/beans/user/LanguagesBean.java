package com.nannieshare.beans.user;

import java.util.ArrayList;
import java.util.List;

public class LanguagesBean {

  List<LanguageBean> languages = new ArrayList<>();

  public List<LanguageBean> getLanguages() {
    return languages;
  }

  public void setLanguages(List<LanguageBean> languages) {
    this.languages = languages;
  }
}
