package com.nannieshare.beans.user;

import com.nannieshare.enums.user.SocialType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserBean extends PublicUserBean implements Serializable {

  private String email;
  private String password;
  private String socialId;
  private SocialType socialProvider;
  private List<RoleBean> roles = new ArrayList<>();
  private List<MediaBean> media = new ArrayList<>();

  // Used with social connectors -> contains token and expiration for client
  private SocialUserBean social;

  /**
   * GETTERS AND SETTERS
   */

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<RoleBean> getRoles() {
    return roles;
  }

  public void setRoles(List<RoleBean> roles) {
    this.roles = roles;
  }

  public SocialType getSocialProvider() {
    return socialProvider;
  }

  public void setSocialProvider(SocialType socialProvider) {
    this.socialProvider = socialProvider;
  }

  public String getSocialId() {
    return socialId;
  }

  public void setSocialId(String socialId) {
    this.socialId = socialId;
  }

  public List<MediaBean> getMedia() {
    return media;
  }

  public void setMedia(List<MediaBean> media) {
    this.media = media;
  }

  public SocialUserBean getSocial() {
    return social;
  }

  public void setSocial(SocialUserBean social) {
    this.social = social;
  }
}
