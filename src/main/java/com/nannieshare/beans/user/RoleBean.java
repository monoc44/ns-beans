package com.nannieshare.beans.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nannieshare.enums.user.RoleType;

import java.io.Serializable;

public class RoleBean implements Serializable {

  private RoleType roleType;
  private String startTime;
  private String endTime;

  @JsonIgnore
  private String grantSecretKey;


  /**
   * GETTERS AND SETTERS
   */

  public RoleType getRoleType() {
    return roleType;
  }

  public String getGrantSecretKey() {
    return grantSecretKey;
  }

  public void setGrantSecretKey(String grantSecretKey) {
    this.grantSecretKey = grantSecretKey;
  }

  public void setRoleType(RoleType roleType) {
    this.roleType = roleType;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }
}
