package com.nannieshare.beans.user;

public class OpeningBean {

  private String zipCode;
  private String openingDate;

  /**
   * GETTERS AND SETTERS
   */

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getOpeningDate() {
    return openingDate;
  }

  public void setOpeningDate(String openingDate) {
    this.openingDate = openingDate;
  }
}
