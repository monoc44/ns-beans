package com.nannieshare.beans.user;

import com.nannieshare.enums.user.UserType;

import java.io.Serializable;

public class PublicUserBean implements Serializable {

  private Integer userId;
  private UserType type;
  private String lastName;
  private String firstName;
  private String lastConnection;

  // Used to validate input information with reCAPTCHA security check
  private String recaptchaToken;

  /**
   * GETTERS AND SETTERS
   */

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public UserType getType() {
    return type;
  }

  public void setType(UserType type) {
    this.type = type;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastConnection() {
    return lastConnection;
  }

  public void setLastConnection(String lastConnection) {
    this.lastConnection = lastConnection;
  }

  public String getRecaptchaToken() {
    return recaptchaToken;
  }

  public void setRecaptchaToken(String recaptchaToken) {
    this.recaptchaToken = recaptchaToken;
  }
}
