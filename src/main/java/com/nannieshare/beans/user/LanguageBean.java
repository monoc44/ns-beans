package com.nannieshare.beans.user;

import com.nannieshare.enums.nanny.LanguageLevelType;
import com.nannieshare.enums.nanny.LanguageType;

import java.io.Serializable;

public class LanguageBean implements Serializable {

  private LanguageType language;
  private String name;
  private LanguageLevelType languageLevel;

  public LanguageType getLanguage() {
    return language;
  }

  public void setLanguage(LanguageType language) {
    this.language = language;
  }

  public LanguageLevelType getLanguageLevel() {
    return languageLevel;
  }

  public void setLanguageLevel(LanguageLevelType languageLevel) {
    this.languageLevel = languageLevel;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
