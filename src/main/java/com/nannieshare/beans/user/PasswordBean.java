package com.nannieshare.beans.user;

public class PasswordBean {

  private String currentPassword;
  private String newPassword;

  /**
   * GETTERS AND SETTERS
   */

  public String getCurrentPassword() {
    return currentPassword;
  }

  public void setCurrentPassword(String currentPassword) {
    this.currentPassword = currentPassword;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }
}
