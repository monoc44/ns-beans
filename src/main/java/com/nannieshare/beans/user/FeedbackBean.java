package com.nannieshare.beans.user;

public class FeedbackBean {

  private Integer userId;
  private Integer reason;
  private String reasonComment;
  private Integer serviceRating;
  private Integer recommendationRating;

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Integer getReason() {
    return reason;
  }

  public void setReason(Integer reason) {
    this.reason = reason;
  }

  public String getReasonComment() {
    return reasonComment;
  }

  public void setReasonComment(String reasonComment) {
    this.reasonComment = reasonComment;
  }

  public Integer getServiceRating() {
    return serviceRating;
  }

  public void setServiceRating(Integer serviceRating) {
    this.serviceRating = serviceRating;
  }

  public Integer getRecommendationRating() {
    return recommendationRating;
  }

  public void setRecommendationRating(Integer recommendationRating) {
    this.recommendationRating = recommendationRating;
  }
}
