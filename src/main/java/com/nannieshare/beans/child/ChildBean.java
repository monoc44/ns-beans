package com.nannieshare.beans.child;

import com.nannieshare.beans.user.MediaBean;
import com.nannieshare.enums.user.GenderType;

import javax.validation.constraints.Pattern;

public class ChildBean {

  private Integer publicId;
  private Integer parentUserId;
  private String name;
  private GenderType gender;
  private String sleep;
  private String cleaning;
  private String food;
  private String allergies;
  private String other;
  private String emergency;
  private String medical;
  private MediaBean media;

  @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$")
  private String dob;


  /**
   * GETTERS AND SETTERS
   */

  public Integer getPublicId() {
    return publicId;
  }

  public void setPublicId(Integer publicId) {
    this.publicId = publicId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }

  public Integer getParentUserId() {
    return parentUserId;
  }

  public void setParentUserId(Integer parentUserId) {
    this.parentUserId = parentUserId;
  }

  public String getSleep() {
    return sleep;
  }

  public void setSleep(String sleep) {
    this.sleep = sleep;
  }

  public String getCleaning() {
    return cleaning;
  }

  public void setCleaning(String cleaning) {
    this.cleaning = cleaning;
  }

  public String getFood() {
    return food;
  }

  public void setFood(String food) {
    this.food = food;
  }

  public String getAllergies() {
    return allergies;
  }

  public void setAllergies(String allergies) {
    this.allergies = allergies;
  }

  public String getOther() {
    return other;
  }

  public void setOther(String other) {
    this.other = other;
  }

  public MediaBean getMedia() {
    return media;
  }

  public void setMedia(MediaBean media) {
    this.media = media;
  }

  public String getEmergency() {
    return emergency;
  }

  public void setEmergency(String emergency) {
    this.emergency = emergency;
  }

  public String getMedical() {
    return medical;
  }

  public void setMedical(String medical) {
    this.medical = medical;
  }
}
