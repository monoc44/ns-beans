package com.nannieshare.beans.child;

import com.nannieshare.beans.user.MediaBean;
import com.nannieshare.enums.user.GenderType;

public class PublicChildBean {

  private String name;
  private Integer ageInMonths;
  private GenderType gender;
  private MediaBean media;

  /**
   * GETTERS AND SETTERS
   */

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAgeInMonths() {
    return ageInMonths;
  }

  public void setAgeInMonths(Integer ageInMonths) {
    this.ageInMonths = ageInMonths;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }

  public MediaBean getMedia() {
    return media;
  }

  public void setMedia(MediaBean media) {
    this.media = media;
  }
}
