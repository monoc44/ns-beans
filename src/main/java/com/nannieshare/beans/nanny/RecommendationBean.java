package com.nannieshare.beans.nanny;

import com.nannieshare.enums.user.UserType;

public class RecommendationBean {

  // Referrer is a NS user
  private Integer referrerUserId;
  private UserType referrerType;

  // Referrer is an outsider
  private String referrerName;
  private String referrerEmail;
  private String referrerPhoneNumber;
  private String referrerSourceURL;

  // Nanny is a NS user
  private Integer nannyUserId;

  // Nanny is yet an outsider
  private String nannyLastName;
  private String nannyFirstName;
  private String nannyEmail;
  private String nannyPhoneNumber;

  private String postedTime;

  private String review;

  private String recaptchaToken;
  private String inviteToken;

  public Integer getReferrerUserId() {
    return referrerUserId;
  }

  public void setReferrerUserId(Integer referrerUserId) {
    this.referrerUserId = referrerUserId;
  }

  public UserType getReferrerType() {
    return referrerType;
  }

  public void setReferrerType(UserType referrerType) {
    this.referrerType = referrerType;
  }

  public String getReferrerName() {
    return referrerName;
  }

  public void setReferrerName(String referrerName) {
    this.referrerName = referrerName;
  }

  public String getReferrerEmail() {
    return referrerEmail;
  }

  public void setReferrerEmail(String referrerEmail) {
    this.referrerEmail = referrerEmail;
  }

  public String getReferrerPhoneNumber() {
    return referrerPhoneNumber;
  }

  public void setReferrerPhoneNumber(String referrerPhoneNumber) {
    this.referrerPhoneNumber = referrerPhoneNumber;
  }

  public String getReferrerSourceURL() {
    return referrerSourceURL;
  }

  public void setReferrerSourceURL(String referrerSourceURL) {
    this.referrerSourceURL = referrerSourceURL;
  }

  public Integer getNannyUserId() {
    return nannyUserId;
  }

  public void setNannyUserId(Integer nannyUserId) {
    this.nannyUserId = nannyUserId;
  }

  public String getNannyLastName() {
    return nannyLastName;
  }

  public void setNannyLastName(String nannyLastName) {
    this.nannyLastName = nannyLastName;
  }

  public String getNannyFirstName() {
    return nannyFirstName;
  }

  public void setNannyFirstName(String nannyFirstName) {
    this.nannyFirstName = nannyFirstName;
  }

  public String getNannyEmail() {
    return nannyEmail;
  }

  public void setNannyEmail(String nannyEmail) {
    this.nannyEmail = nannyEmail;
  }

  public String getNannyPhoneNumber() {
    return nannyPhoneNumber;
  }

  public void setNannyPhoneNumber(String nannyPhoneNumber) {
    this.nannyPhoneNumber = nannyPhoneNumber;
  }

  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }

  public String getPostedTime() {
    return postedTime;
  }

  public void setPostedTime(String postedTime) {
    this.postedTime = postedTime;
  }

  public String getRecaptchaToken() {
    return recaptchaToken;
  }

  public void setRecaptchaToken(String recaptchaToken) {
    this.recaptchaToken = recaptchaToken;
  }

  public String getInviteToken() {
    return inviteToken;
  }

  public void setInviteToken(String inviteToken) {
    this.inviteToken = inviteToken;
  }
}
