package com.nannieshare.beans.nanny;

import com.nannieshare.beans.address.AddressBean;
import com.nannieshare.beans.project.CriterionBean;
import com.nannieshare.beans.user.LanguageBean;
import com.nannieshare.beans.user.MediaBean;
import com.nannieshare.beans.user.UserBean;
import com.nannieshare.enums.user.GenderType;

import java.util.ArrayList;
import java.util.List;

/**
 * Nanny user info bean used by UI to retrieve basic
 * information of connected nanny.
 */
public class NannyBean extends UserBean {

  private AddressBean address;
  private Boolean visibleOnMap;

  private List<MediaBean> media = new ArrayList<>();
  private List<RecommendationBean> recommendation = new ArrayList<>();
  private List<LanguageBean> spokenLanguages = new ArrayList<>();
  private List<CriterionBean> workCriteria = new ArrayList<>();
  private List<ExperienceBean> experience = new ArrayList<>();
  private List<CertificationBean> certifications = new ArrayList<>();

  private String dob;
  private int yearsExperience;
  private GenderType gender;

  private String aboutMe;
  private String education;
  private Boolean hasCar;
  private Boolean hasPets;

  // only filled up by first signup
  private String inviteToken;

  /**
   * GETTERS / SETTERS
   */

  public AddressBean getAddress() {
    return address;
  }

  public void setAddress(AddressBean address) {
    this.address = address;
  }

  @Override
  public List<MediaBean> getMedia() {
    return media;
  }

  @Override
  public void setMedia(List<MediaBean> media) {
    this.media = media;
  }

  public List<RecommendationBean> getRecommendation() {
    return recommendation;
  }

  public void setRecommendation(List<RecommendationBean> recommendation) {
    this.recommendation = recommendation;
  }

  public List<LanguageBean> getSpokenLanguages() {
    return spokenLanguages;
  }

  public void setSpokenLanguages(List<LanguageBean> spokenLanguages) {
    this.spokenLanguages = spokenLanguages;
  }

  public List<CriterionBean> getWorkCriteria() {
    return workCriteria;
  }

  public void setWorkCriteria(List<CriterionBean> workCriteria) {
    this.workCriteria = workCriteria;
  }

  public List<ExperienceBean> getExperience() {
    return experience;
  }

  public void setExperience(List<ExperienceBean> experience) {
    this.experience = experience;
  }

  public List<CertificationBean> getCertifications() {
    return certifications;
  }

  public void setCertifications(List<CertificationBean> certifications) {
    this.certifications = certifications;
  }

  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public int getYearsExperience() {
    return yearsExperience;
  }

  public void setYearsExperience(int yearsExperience) {
    this.yearsExperience = yearsExperience;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }

  public String getAboutMe() {
    return aboutMe;
  }

  public void setAboutMe(String aboutMe) {
    this.aboutMe = aboutMe;
  }

  public String getEducation() {
    return education;
  }

  public void setEducation(String education) {
    this.education = education;
  }

  public Boolean getHasCar() {
    return hasCar;
  }

  public void setHasCar(Boolean hasCar) {
    this.hasCar = hasCar;
  }

  public String getInviteToken() {
    return inviteToken;
  }

  public void setInviteToken(String inviteToken) {
    this.inviteToken = inviteToken;
  }

  public Boolean getVisibleOnMap() {
    return visibleOnMap;
  }

  public void setVisibleOnMap(Boolean visibleOnMap) {
    this.visibleOnMap = visibleOnMap;
  }

  public Boolean getHasPets() {
    return hasPets;
  }

  public void setHasPets(Boolean hasPets) {
    this.hasPets = hasPets;
  }
}