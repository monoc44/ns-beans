package com.nannieshare.beans.nanny;

import com.nannieshare.beans.address.PublicAddressBean;
import com.nannieshare.beans.project.CriterionBean;
import com.nannieshare.beans.user.LanguageBean;
import com.nannieshare.beans.user.MediaBean;
import com.nannieshare.beans.user.PublicUserBean;
import com.nannieshare.enums.user.GenderType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PublicNannyBean extends PublicUserBean implements Serializable {

  private PublicAddressBean address;

  private List<MediaBean> media = new ArrayList<>();
  private List<RecommendationBean> recommendation = new ArrayList<>();
  private List<LanguageBean> spokenLanguages = new ArrayList<>();
  private List<ExperienceBean> experience = new ArrayList<>();
  private List<CertificationBean> certifications = new ArrayList<>();
  private List<CriterionBean> workCriteria = new ArrayList<>();

  private String aboutMe;
  private String education;
  private Integer age;
  private Boolean hasCar;
  private Boolean hasPets;
  private int yearsExperience;
  private GenderType gender;
  private Integer responseTimeInHours;

  public List<LanguageBean> getSpokenLanguages() {
    return spokenLanguages;
  }

  public void setSpokenLanguages(List<LanguageBean> spokenLanguages) {
    this.spokenLanguages = spokenLanguages;
  }

  public String getEducation() {
    return education;
  }

  public void setEducation(String education) {
    this.education = education;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public Boolean getHasCar() {
    return hasCar;
  }

  public void setHasCar(Boolean hasCar) {
    this.hasCar = hasCar;
  }

  public Integer getResponseTimeInHours() {
    return responseTimeInHours;
  }

  public void setResponseTimeInHours(Integer responseTimeInHours) {
    this.responseTimeInHours = responseTimeInHours;
  }

  public PublicAddressBean getAddress() {
    return address;
  }

  public void setAddress(PublicAddressBean address) {
    this.address = address;
  }

  public List<MediaBean> getMedia() {
    return media;
  }

  public void setMedia(List<MediaBean> media) {
    this.media = media;
  }

  public List<RecommendationBean> getRecommendation() {
    return recommendation;
  }

  public void setRecommendation(List<RecommendationBean> recommendation) {
    this.recommendation = recommendation;
  }

  public String getAboutMe() {
    return aboutMe;
  }

  public void setAboutMe(String aboutMe) {
    this.aboutMe = aboutMe;
  }

  public List<ExperienceBean> getExperience() {
    return experience;
  }

  public void setExperience(List<ExperienceBean> experience) {
    this.experience = experience;
  }

  public List<CertificationBean> getCertifications() {
    return certifications;
  }

  public void setCertifications(List<CertificationBean> certifications) {
    this.certifications = certifications;
  }

  public int getYearsExperience() {
    return yearsExperience;
  }

  public void setYearsExperience(int yearsExperience) {
    this.yearsExperience = yearsExperience;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }

  public List<CriterionBean> getWorkCriteria() {
    return workCriteria;
  }

  public void setWorkCriteria(List<CriterionBean> workCriteria) {
    this.workCriteria = workCriteria;
  }

  public Boolean getHasPets() {
    return hasPets;
  }

  public void setHasPets(Boolean hasPets) {
    this.hasPets = hasPets;
  }
}
