package com.nannieshare.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ApiError implements Serializable {

  private int statusCode;
  private Date timestamp = new Date();
  private String message;
  private List<String> errors;

  public ApiError(int statusCode, String message, String error) {
    this(statusCode, message, Arrays.asList(error));
  }

  public ApiError(int statusCode, String message, List<String> errors) {
    Objects.requireNonNull(statusCode, "Status code can't be null");
    Objects.requireNonNull(message, "Message can't be null");
    Objects.requireNonNull(errors, "List of errors can't be null");
    this.statusCode = statusCode;
    this.message = message;
    this.errors = errors;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public String getMessage() {
    return message;
  }

  public List<String> getErrors() {
    return errors;
  }

  public void setErrors(List<String> errors) {
    this.errors = errors;
  }
}