package com.nannieshare.beans.daycare;

import com.nannieshare.beans.address.AddressBean;
import com.nannieshare.enums.daycare.LicenseStatus;
import com.nannieshare.enums.map.ChildCareType;
import com.nannieshare.enums.user.UserType;

import java.io.Serializable;

public class DayCareOnMapBean implements Serializable {

  private final UserType type = UserType.DAYCARE;
  private Integer daycareId;
  private ChildCareType careType;
  private String name;
  private String phoneNumber;
  private AddressBean address;
  private LicenseStatus status;
  private short capacity;

  public UserType getType() {
    return type;
  }

  public Integer getId() {
    return daycareId;
  }

  public Integer getDaycareId() {
    return daycareId;
  }

  public void setDaycareId(Integer daycareId) {
    this.daycareId = daycareId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public AddressBean getAddress() {
    return address;
  }

  public void setAddress(AddressBean address) {
    this.address = address;
  }

  public short getCapacity() {
    return capacity;
  }

  public void setCapacity(short capacity) {
    this.capacity = capacity;
  }

  public LicenseStatus getStatus() {
    return status;
  }

  public void setStatus(LicenseStatus status) {
    this.status = status;
  }

  public ChildCareType getCareType() {
    return careType;
  }

  public void setCareType(ChildCareType careType) {
    this.careType = careType;
  }
}