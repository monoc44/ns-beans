package com.nannieshare.beans.daycare;

import com.nannieshare.beans.address.AddressBean;
import com.nannieshare.beans.user.UserBean;
import com.nannieshare.enums.user.UserType;

import javax.validation.constraints.NotBlank;

/**
 * Daycare user info bean used by UI to retrieve basic
 * information of connected daycare.
 */
public class DaycareUserBean extends UserBean {

  @NotBlank
  private Integer daycareId;

  @NotBlank
  private String name;

  @Override
  public UserType getType() {
    return UserType.DAYCARE;
  }

  private AddressBean address;

  public Integer getDaycareId() {
    return daycareId;
  }

  public void setDaycareId(Integer daycareId) {
    this.daycareId = daycareId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AddressBean getAddress() {
    return address;
  }

  public void setAddress(AddressBean address) {
    this.address = address;
  }
}