package com.nannieshare.beans.map;

import com.nannieshare.beans.daycare.DayCareOnMapBean;
import com.nannieshare.beans.user.LinkBean;

import java.util.ArrayList;
import java.util.List;

public class MapResultBean {

  private List<ParentOnMapBean> parents = new ArrayList<>();
  private List<NannyOnMapBean> nannies = new ArrayList<>();
  private List<DayCareOnMapBean> daycares = new ArrayList<>();
  private List<LinkBean> links = new ArrayList<>();

  /**
   * GETTERS AND SETTERS
   */

  public List<DayCareOnMapBean> getDaycares() {
    return daycares;
  }

  public void setDaycares(List<DayCareOnMapBean> daycares) {
    this.daycares = daycares;
  }

  public List<ParentOnMapBean> getParents() {
    return parents;
  }

  public void setParents(List<ParentOnMapBean> parents) {
    this.parents = parents;
  }

  public List<LinkBean> getLinks() {
    return links;
  }

  public void setLinks(List<LinkBean> links) {
    this.links = links;
  }

  public List<NannyOnMapBean> getNannies() {
    return nannies;
  }

  public void setNannies(List<NannyOnMapBean> nannies) {
    this.nannies = nannies;
  }
}
