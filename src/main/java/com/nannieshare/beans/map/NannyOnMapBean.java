package com.nannieshare.beans.map;

import com.nannieshare.beans.address.PublicAddressBean;
import com.nannieshare.beans.user.LanguageBean;
import com.nannieshare.beans.user.PublicUserBean;
import com.nannieshare.enums.map.ChildCareType;
import com.nannieshare.enums.user.GenderType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NannyOnMapBean extends PublicUserBean implements Serializable {

  private ChildCareType careType;
  private PublicAddressBean address;
  private GenderType gender;
  private Integer age;
  private int yearsExperience;
  private int nbCertifications;
  private int nbRecommendations;
  private List<LanguageBean> spokenLanguages = new ArrayList<>();

  /**
   * GETTERS AND SETTERS
   */

  public ChildCareType getCareType() {
    return careType;
  }

  public void setCareType(ChildCareType careType) {
    this.careType = careType;
  }

  public PublicAddressBean getAddress() {
    return address;
  }

  public void setAddress(PublicAddressBean address) {
    this.address = address;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public int getYearsExperience() {
    return yearsExperience;
  }

  public void setYearsExperience(int yearsExperience) {
    this.yearsExperience = yearsExperience;
  }

  public int getNbCertifications() {
    return nbCertifications;
  }

  public void setNbCertifications(int nbCertifications) {
    this.nbCertifications = nbCertifications;
  }

  public int getNbRecommendations() {
    return nbRecommendations;
  }

  public void setNbRecommendations(int nbRecommendations) {
    this.nbRecommendations = nbRecommendations;
  }

  public List<LanguageBean> getSpokenLanguages() {
    return spokenLanguages;
  }

  public void setSpokenLanguages(List<LanguageBean> spokenLanguages) {
    this.spokenLanguages = spokenLanguages;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }
}
