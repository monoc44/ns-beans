package com.nannieshare.beans.map;

import com.nannieshare.enums.map.ChildCareType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchMapBean implements Serializable {

  @NotNull
  private Float northEastLatitude;

  @NotNull
  private Float northEastLongitude;

  @NotNull
  private Float southWestLatitude;

  @NotNull
  private Float southWestLongitude;

  private List<ChildCareType> interestedBy = new ArrayList<>();

  public Float getNorthEastLatitude() {
    return northEastLatitude;
  }

  public void setNorthEastLatitude(Float northEastLatitude) {
    this.northEastLatitude = northEastLatitude;
  }

  public Float getNorthEastLongitude() {
    return northEastLongitude;
  }

  public void setNorthEastLongitude(Float northEastLongitude) {
    this.northEastLongitude = northEastLongitude;
  }

  public Float getSouthWestLatitude() {
    return southWestLatitude;
  }

  public void setSouthWestLatitude(Float southWestLatitude) {
    this.southWestLatitude = southWestLatitude;
  }

  public Float getSouthWestLongitude() {
    return southWestLongitude;
  }

  public void setSouthWestLongitude(Float southWestLongitude) {
    this.southWestLongitude = southWestLongitude;
  }

  public List<ChildCareType> getInterestedBy() {
    return interestedBy;
  }

  public void setInterestedBy(List<ChildCareType> interestedBy) {
    this.interestedBy = interestedBy;
  }
}
