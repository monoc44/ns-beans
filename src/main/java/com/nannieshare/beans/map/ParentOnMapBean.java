package com.nannieshare.beans.map;

import com.nannieshare.beans.address.PublicAddressBean;
import com.nannieshare.beans.child.PublicChildBean;
import com.nannieshare.beans.parent.ScheduleDayBean;
import com.nannieshare.beans.user.PublicUserBean;
import com.nannieshare.enums.map.ChildCareType;
import com.nannieshare.enums.parent.HostingType;

import java.io.Serializable;
import java.util.List;

public class ParentOnMapBean extends PublicUserBean implements Serializable {

  private ChildCareType careType;
  private Integer projectId;
  private boolean needNanny;
  private boolean needFamily;
  private HostingType hosting;
  private PublicAddressBean address;
  private List<PublicChildBean> children;
  private List<ScheduleDayBean> agenda;
  private String startDate;
  private String endDate;
  private int agendaMatchingScore;

  /**
   * GETTERS AND SETTERS
   */

  public Integer getProjectId() {
    return projectId;
  }

  public void setProjectId(Integer projectId) {
    this.projectId = projectId;
  }

  public PublicAddressBean getAddress() {
    return address;
  }

  public void setAddress(PublicAddressBean address) {
    this.address = address;
  }

  public boolean isNeedNanny() {
    return needNanny;
  }

  public void setNeedNanny(boolean needNanny) {
    this.needNanny = needNanny;
  }

  public boolean isNeedFamily() {
    return needFamily;
  }

  public void setNeedFamily(boolean needFamily) {
    this.needFamily = needFamily;
  }

  public ChildCareType getCareType() {
    return careType;
  }

  public void setCareType(ChildCareType careType) {
    this.careType = careType;
  }

  public HostingType getHosting() {
    return hosting;
  }

  public void setHosting(HostingType hosting) {
    this.hosting = hosting;
  }

  public List<PublicChildBean> getChildren() {
    return children;
  }

  public void setChildren(List<PublicChildBean> children) {
    this.children = children;
  }

  public int getAgendaMatchingScore() {
    return agendaMatchingScore;
  }

  public void setAgendaMatchingScore(int agendaMatchingScore) {
    this.agendaMatchingScore = agendaMatchingScore;
  }

  public List<ScheduleDayBean> getAgenda() {
    return agenda;
  }

  public void setAgenda(List<ScheduleDayBean> agenda) {
    this.agenda = agenda;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
}
