package com.nannieshare.beans.billing;

import com.nannieshare.enums.billing.CardBrandType;
import com.nannieshare.enums.billing.CardFundingType;

import java.io.Serializable;

public class CardSourceBean implements Serializable {

  private CardBrandType brand;
  private String last4;
  private Long expMonth;
  private Long expYear;
  private Boolean expired;
  private CardFundingType funding;

  public CardBrandType getBrand() {
    return brand;
  }

  public void setBrand(CardBrandType brand) {
    this.brand = brand;
  }

  public String getLast4() {
    return last4;
  }

  public void setLast4(String last4) {
    this.last4 = last4;
  }

  public Long getExpMonth() {
    return expMonth;
  }

  public void setExpMonth(Long expMonth) {
    this.expMonth = expMonth;
  }

  public Long getExpYear() {
    return expYear;
  }

  public void setExpYear(Long expYear) {
    this.expYear = expYear;
  }

  public Boolean getExpired() {
    return expired;
  }

  public void setExpired(Boolean expired) {
    this.expired = expired;
  }

  public CardFundingType getFunding() {
    return funding;
  }

  public void setFunding(CardFundingType funding) {
    this.funding = funding;
  }
}
