package com.nannieshare.beans.billing;

import com.nannieshare.enums.billing.SourceStatusType;
import com.nannieshare.enums.user.UserType;

import java.io.Serializable;

public class SourceBean implements Serializable {

  private Integer userId;
  private UserType userType;

  private String sourceId;
  private String sourceClientSecret;
  private String sourceOwnerZipCode;

  private SourceStatusType status;

  private Boolean isNew;
  private Boolean isDefault;

  // If source is a card, information will be provided here
  private CardSourceBean card;


  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public UserType getUserType() {
    return userType;
  }

  public void setUserType(UserType userType) {
    this.userType = userType;
  }

  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public String getSourceClientSecret() {
    return sourceClientSecret;
  }

  public void setSourceClientSecret(String sourceClientSecret) {
    this.sourceClientSecret = sourceClientSecret;
  }

  public String getSourceOwnerZipCode() {
    return sourceOwnerZipCode;
  }

  public void setSourceOwnerZipCode(String sourceOwnerZipCode) {
    this.sourceOwnerZipCode = sourceOwnerZipCode;
  }

  public SourceStatusType getStatus() {
    return status;
  }

  public void setStatus(SourceStatusType status) {
    this.status = status;
  }

  public Boolean getNew() {
    return isNew;
  }

  public void setNew(Boolean aNew) {
    isNew = aNew;
  }

  public Boolean getDefault() {
    return isDefault;
  }

  public void setDefault(Boolean aDefault) {
    isDefault = aDefault;
  }

  public CardSourceBean getCard() {
    return card;
  }

  public void setCard(CardSourceBean card) {
    this.card = card;
  }
}
