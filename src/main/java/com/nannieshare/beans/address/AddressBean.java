package com.nannieshare.beans.address;

import java.io.Serializable;

public class AddressBean implements Serializable {

  private String formattedAddress;
  private String street;
  private String neighborhood;
  private String city;
  private String county;
  private String zipCode;
  private String zipCodeSuffix;
  private String state;
  private String country;

  private GeocodingBean privateGeocoding;
  private GeocodingBean publicGeocoding;

  public String getFormattedAddress() {
    return formattedAddress;
  }

  public void setFormattedAddress(String formattedAddress) {
    this.formattedAddress = formattedAddress;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getNeighborhood() {
    return neighborhood;
  }

  public void setNeighborhood(String neighborhood) {
    this.neighborhood = neighborhood;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getZipCodeSuffix() {
    return zipCodeSuffix;
  }

  public void setZipCodeSuffix(String zipCodeSuffix) {
    this.zipCodeSuffix = zipCodeSuffix;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public GeocodingBean getPrivateGeocoding() {
    return privateGeocoding;
  }

  public void setPrivateGeocoding(GeocodingBean privateGeocoding) {
    this.privateGeocoding = privateGeocoding;
  }

  public GeocodingBean getPublicGeocoding() {
    return publicGeocoding;
  }

  public void setPublicGeocoding(GeocodingBean publicGeocoding) {
    this.publicGeocoding = publicGeocoding;
  }
}
