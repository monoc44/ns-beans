package com.nannieshare.beans.address;

import java.io.Serializable;

public class PublicAddressBean implements Serializable {

  private String neighborhood;
  private String city;
  private String county;
  private String state;
  private String country;

  private GeocodingBean publicGeocoding;

  public String getNeighborhood() {
    return neighborhood;
  }

  public void setNeighborhood(String neighborhood) {
    this.neighborhood = neighborhood;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public GeocodingBean getPublicGeocoding() {
    return publicGeocoding;
  }

  public void setPublicGeocoding(GeocodingBean publicGeocoding) {
    this.publicGeocoding = publicGeocoding;
  }
}
