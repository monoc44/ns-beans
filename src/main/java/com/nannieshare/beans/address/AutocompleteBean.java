package com.nannieshare.beans.address;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class AutocompleteBean implements Serializable {

  @NotBlank(message = "is compulsory")
  private String address;

  @NotBlank(message = "is compulsory")
  private String placeId;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPlaceId() {
    return placeId;
  }

  public void setPlaceId(String placeId) {
    this.placeId = placeId;
  }
}
