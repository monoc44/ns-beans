package com.nannieshare.beans.address;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class GeocodingBean implements Serializable {

  @NotNull
  private Float lat;

  @NotNull
  private Float lng;

  public static GeocodingBean from(Float latitude, Float longitude) {
    GeocodingBean toReturn = new GeocodingBean();
    toReturn.lat = latitude;
    toReturn.lng = longitude;
    return toReturn;
  }

  public Float getLat() {
    return lat;
  }

  public Float getLng() {
    return lng;
  }

}
