package com.nannieshare.beans.parent;

import com.nannieshare.beans.child.ChildBean;
import com.nannieshare.beans.project.ProjectBean;
import com.nannieshare.beans.project.ProjectIdBean;
import com.nannieshare.beans.user.LinkBean;
import com.nannieshare.beans.user.UserBean;
import com.nannieshare.enums.user.UserType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ParentBean extends UserBean implements Serializable {

  private final UserType type = UserType.PARENT;
  private List<ProjectIdBean> projects;
  private ProjectBean project;
  private List<ChildBean> children = new ArrayList<>();
  private List<LinkBean> links = new ArrayList<>();

  /**
   * GETTERS AND SETTERS
   */

  public List<LinkBean> getLinks() {
    return links;
  }

  public void setLinks(List<LinkBean> links) {
    this.links = links;
  }

  @Override
  public UserType getType() {
    return type;
  }

  public ProjectBean getProject() {
    return project;
  }

  public void setProject(ProjectBean project) {
    this.project = project;
  }

  public List<ProjectIdBean> getProjects() {
    return projects;
  }

  public void setProjects(List<ProjectIdBean> projects) {
    this.projects = projects;
  }

  public List<ChildBean> getChildren() {
    return children;
  }

  public void setChildren(List<ChildBean> children) {
    this.children = children;
  }
}
