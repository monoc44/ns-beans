package com.nannieshare.beans.parent;

import com.nannieshare.enums.user.DayType;

public class ScheduleDayBean {

  private DayType type;
  private Integer startTimeA;
  private Integer endTimeA;
  private Integer startTimeB;
  private Integer endTimeB;

  /**
   * GETTERS AND SETTERS
   */

  public DayType getType() {
    return type;
  }

  public void setType(DayType type) {
    this.type = type;
  }

  public Integer getStartTimeA() {
    return startTimeA;
  }

  public void setStartTimeA(Integer startTimeA) {
    this.startTimeA = startTimeA;
  }

  public Integer getEndTimeA() {
    return endTimeA;
  }

  public void setEndTimeA(Integer endTimeA) {
    this.endTimeA = endTimeA;
  }

  public Integer getStartTimeB() {
    return startTimeB;
  }

  public void setStartTimeB(Integer startTimeB) {
    this.startTimeB = startTimeB;
  }

  public Integer getEndTimeB() {
    return endTimeB;
  }

  public void setEndTimeB(Integer endTimeB) {
    this.endTimeB = endTimeB;
  }
}