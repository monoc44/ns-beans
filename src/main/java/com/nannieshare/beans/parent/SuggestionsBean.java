package com.nannieshare.beans.parent;

import com.nannieshare.beans.map.ParentOnMapBean;

import java.util.List;

public class SuggestionsBean {

  private ParentBean parent;
  private List<ParentOnMapBean> candidates;

  public ParentBean getParent() {
    return parent;
  }

  public void setParent(ParentBean parent) {
    this.parent = parent;
  }

  public List<ParentOnMapBean> getCandidates() {
    return candidates;
  }

  public void setCandidates(List<ParentOnMapBean> candidates) {
    this.candidates = candidates;
  }
}
