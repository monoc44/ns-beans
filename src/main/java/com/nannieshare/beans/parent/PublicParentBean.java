package com.nannieshare.beans.parent;

import com.nannieshare.beans.project.PublicProjectBean;
import com.nannieshare.beans.user.MediaBean;
import com.nannieshare.beans.user.PublicUserBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PublicParentBean extends PublicUserBean implements Serializable {

  private PublicProjectBean project;
  private List<MediaBean> media = new ArrayList<>();

  /**
   * GETTERS AND SETTERS
   */

  public PublicProjectBean getProject() {
    return project;
  }

  public void setProject(PublicProjectBean project) {
    this.project = project;
  }

  public List<MediaBean> getMedia() {
    return media;
  }

  public void setMedia(List<MediaBean> media) {
    this.media = media;
  }
}
