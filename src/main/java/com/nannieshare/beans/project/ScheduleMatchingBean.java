package com.nannieshare.beans.project;

import java.io.Serializable;

public class ScheduleMatchingBean implements Serializable {

  private Integer overlapScore;

  private Integer weeklyBudgetAlone;
  private Integer weeklyBudgetWithMyRate;
  private Integer weeklyBudgetWithTheirRate;

  public Integer getOverlapScore() {
    return overlapScore;
  }

  public void setOverlapScore(Integer overlapScore) {
    this.overlapScore = overlapScore;
  }

  public Integer getWeeklyBudgetWithMyRate() {
    return weeklyBudgetWithMyRate;
  }

  public void setWeeklyBudgetWithMyRate(Integer weeklyBudgetWithMyRate) {
    this.weeklyBudgetWithMyRate = weeklyBudgetWithMyRate;
  }

  public Integer getWeeklyBudgetWithTheirRate() {
    return weeklyBudgetWithTheirRate;
  }

  public void setWeeklyBudgetWithTheirRate(Integer weeklyBudgetWithTheirRate) {
    this.weeklyBudgetWithTheirRate = weeklyBudgetWithTheirRate;
  }

  public Integer getWeeklyBudgetAlone() {
    return weeklyBudgetAlone;
  }

  public void setWeeklyBudgetAlone(Integer weeklyBudgetAlone) {
    this.weeklyBudgetAlone = weeklyBudgetAlone;
  }
}
