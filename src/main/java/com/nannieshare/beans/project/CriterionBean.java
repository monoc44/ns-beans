package com.nannieshare.beans.project;

import com.nannieshare.enums.project.CriterionType;

public class CriterionBean {

  private CriterionType type;
  private int value;
  private boolean flexible;

  /**
   * GETTERS AND SETTERS
   */

  public CriterionType getType() {
    return type;
  }

  public void setType(CriterionType type) {
    this.type = type;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public boolean isFlexible() {
    return flexible;
  }

  public void setFlexible(boolean flexible) {
    this.flexible = flexible;
  }
}
