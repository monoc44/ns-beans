package com.nannieshare.beans.project;

import com.nannieshare.beans.user.OpeningStatusBean;
import com.nannieshare.enums.project.ColorType;
import com.nannieshare.enums.project.IconType;

import java.io.Serializable;

public class ProjectIdBean implements Serializable {

  private Integer projectId;
  private String name;
  private IconType icon;
  private ColorType iconColor;
  private OpeningStatusBean openingStatus;

  /**
   * GETTERS AND SETTERS
   */

  public Integer getProjectId() {
    return projectId;
  }

  public void setProjectId(Integer projectId) {
    this.projectId = projectId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public IconType getIcon() {
    return icon;
  }

  public void setIcon(IconType icon) {
    this.icon = icon;
  }

  public ColorType getIconColor() {
    return iconColor;
  }

  public void setIconColor(ColorType iconColor) {
    this.iconColor = iconColor;
  }

  public OpeningStatusBean getOpeningStatus() {
    return openingStatus;
  }

  public void setOpeningStatus(OpeningStatusBean openingStatus) {
    this.openingStatus = openingStatus;
  }
}
