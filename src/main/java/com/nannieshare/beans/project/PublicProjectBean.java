package com.nannieshare.beans.project;

import com.nannieshare.beans.address.PublicAddressBean;
import com.nannieshare.beans.child.PublicChildBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PublicProjectBean implements Serializable {

  private Integer projectId;
  private String startDate;
  private String endDate;
  private String description;
  private Boolean hasPets;
  private PublicAddressBean address;
  private ScheduleMatchingBean scheduleMatching;

  private List<PublicChildBean> children = new ArrayList<>();
  private List<CriterionBean> criteria = new ArrayList<>();

  /**
   * GETTERS AND SETTERS
   */

  public Integer getProjectId() {
    return projectId;
  }

  public void setProjectId(Integer projectId) {
    this.projectId = projectId;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PublicAddressBean getAddress() {
    return address;
  }

  public void setAddress(PublicAddressBean address) {
    this.address = address;
  }

  public List<PublicChildBean> getChildren() {
    return children;
  }

  public void setChildren(List<PublicChildBean> children) {
    this.children = children;
  }

  public List<CriterionBean> getCriteria() {
    return criteria;
  }

  public void setCriteria(List<CriterionBean> criteria) {
    this.criteria = criteria;
  }

  public Boolean getHasPets() {
    return hasPets;
  }

  public void setHasPets(Boolean hasPets) {
    this.hasPets = hasPets;
  }

  public ScheduleMatchingBean getScheduleMatching() {
    return scheduleMatching;
  }

  public void setScheduleMatching(ScheduleMatchingBean scheduleMatching) {
    this.scheduleMatching = scheduleMatching;
  }
}
