package com.nannieshare.beans.project;

import com.nannieshare.beans.address.AddressBean;

import java.util.ArrayList;
import java.util.List;

public class ProjectBean extends ProjectIdBean {

  private boolean active;
  private String startDate;
  private String endDate;
  private String description;
  private Boolean hasPets;
  private AddressBean address;
  private List<Integer> childrenIds = new ArrayList<>();
  private List<CriterionBean> criteria = new ArrayList<>();

  /**
   * GETTERS AND SETTERS
   */

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public List<CriterionBean> getCriteria() {
    return criteria;
  }

  public void setCriteria(List<CriterionBean> criteria) {
    this.criteria = criteria;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public AddressBean getAddress() {
    return address;
  }

  public void setAddress(AddressBean address) {
    this.address = address;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public List<Integer> getChildrenIds() {
    return childrenIds;
  }

  public void setChildrenIds(List<Integer> childrenIds) {
    this.childrenIds = childrenIds;
  }

  public Boolean getHasPets() {
    return hasPets;
  }

  public void setHasPets(Boolean hasPets) {
    this.hasPets = hasPets;
  }
}
