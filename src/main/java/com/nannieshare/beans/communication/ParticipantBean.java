package com.nannieshare.beans.communication;

import com.nannieshare.beans.user.PublicUserBean;

import java.io.Serializable;

public class ParticipantBean extends PublicUserBean implements Serializable {

  // Attribute used for parents: which project id this participation is in
  private Integer projectId;

  private boolean hasReadLastMessage;

  public Integer getProjectId() {
    return projectId;
  }

  public void setProjectId(Integer projectId) {
    this.projectId = projectId;
  }

  public boolean isHasReadLastMessage() {
    return hasReadLastMessage;
  }

  public void setHasReadLastMessage(boolean hasReadLastMessage) {
    this.hasReadLastMessage = hasReadLastMessage;
  }
}
