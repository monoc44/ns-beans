package com.nannieshare.beans.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ConversationIdBean implements Serializable {

  private Long id;
  private List<ParticipantBean> participants = new ArrayList<>();
  private Boolean hasUnreadMessages;
  private String subject;
  private MessageBean lastMessage;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<ParticipantBean> getParticipants() {
    return participants;
  }

  public void setParticipants(List<ParticipantBean> participants) {
    this.participants = participants;
  }

  public Boolean getHasUnreadMessages() {
    return hasUnreadMessages;
  }

  public void setHasUnreadMessages(Boolean hasUnreadMessages) {
    this.hasUnreadMessages = hasUnreadMessages;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public MessageBean getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(MessageBean lastMessage) {
    this.lastMessage = lastMessage;
  }
}
