package com.nannieshare.beans.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MessageBean implements Serializable {

  private ParticipantBean fromUser;

  private List<ParticipantBean> toUsers = new ArrayList<>();

  private Long conversationId;

  private String message;

  private String subject;

  private String postedTime;

  public void addToUser(ParticipantBean to) {
    toUsers.add(to);
  }

  public ParticipantBean getFromUser() {
    return fromUser;
  }

  public void setFromUser(ParticipantBean fromUser) {
    this.fromUser = fromUser;
  }

  public List<ParticipantBean> getToUsers() {
    return toUsers;
  }

  public void setToUsers(List<ParticipantBean> toUsers) {
    this.toUsers = toUsers;
  }

  public Long getConversationId() {
    return conversationId;
  }

  public void setConversationId(Long conversationId) {
    this.conversationId = conversationId;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getPostedTime() {
    return postedTime;
  }

  public void setPostedTime(String postedTime) {
    this.postedTime = postedTime;
  }
}
