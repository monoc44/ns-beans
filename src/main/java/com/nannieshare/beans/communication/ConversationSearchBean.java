package com.nannieshare.beans.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConversationSearchBean implements Serializable {

  /**
   * Search any conversations where 'withParticipants (userIds)' appears as participants
   * (AND logic applied)
   */
  private List<Integer> withParticipants = new ArrayList<>();

  /**
   * Search any conversations where 'createdBy (userIds)' is the creator of the conversation
   */
  private Integer createdBy;

  /**
   * Search any conversations where one of its messages contains one of the words in 'withContent'
   * (OR logic applied)
   */
  private List<String> withContent = new ArrayList<>();

  /**
   * Search any conversations where one of its subjects contains one of the words in 'withSubject'
   * (OR logic applied)
   */
  private List<String> withSubject = new ArrayList<>();

  /**
   * Search any conversations where one of its messages has been posted after (or at) 'withPostedTimeAfter'
   */
  private Date withPostedTimeAfter;

  /**
   * Search any conversations where one of its messages has been posted before (or at) 'withPostedTimeBefore'
   */
  private Date withPostedTimeBefore;

  public List<Integer> getWithParticipants() {
    return withParticipants;
  }

  public void setWithParticipants(List<Integer> withParticipants) {
    this.withParticipants = withParticipants;
  }

  public Integer getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Integer createdBy) {
    this.createdBy = createdBy;
  }

  public List<String> getWithContent() {
    return withContent;
  }

  public void setWithContent(List<String> withContent) {
    this.withContent = withContent;
  }

  public List<String> getWithSubject() {
    return withSubject;
  }

  public void setWithSubject(List<String> withSubject) {
    this.withSubject = withSubject;
  }

  public Date getWithPostedTimeAfter() {
    return withPostedTimeAfter;
  }

  public void setWithPostedTimeAfter(Date withPostedTimeAfter) {
    this.withPostedTimeAfter = withPostedTimeAfter;
  }

  public Date getWithPostedTimeBefore() {
    return withPostedTimeBefore;
  }

  public void setWithPostedTimeBefore(Date withPostedTimeBefore) {
    this.withPostedTimeBefore = withPostedTimeBefore;
  }
}
