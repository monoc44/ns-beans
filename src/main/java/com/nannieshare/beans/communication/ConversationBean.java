package com.nannieshare.beans.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ConversationBean extends ConversationIdBean implements Serializable {

  private String subject;
  private ParticipantBean createdBy;
  private String creationTime;
  private String lastMessageTime;
  private List<MessageBean> messages = new ArrayList<>();
  private boolean notModified;

  public static ConversationBean NOT_MODIFIED() {
    ConversationBean toReturn = new ConversationBean();
    toReturn.setNotModified(true);
    return toReturn;
  }

  @Override
  public String getSubject() {
    return subject;
  }

  @Override
  public void setSubject(String subject) {
    this.subject = subject;
  }

  public ParticipantBean getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(ParticipantBean createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(String creationTime) {
    this.creationTime = creationTime;
  }

  public String getLastMessageTime() {
    return lastMessageTime;
  }

  public void setLastMessageTime(String lastMessageTime) {
    this.lastMessageTime = lastMessageTime;
  }

  public List<MessageBean> getMessages() {
    return messages;
  }

  public void setMessages(List<MessageBean> messages) {
    this.messages = messages;
  }

  public boolean isNotModified() {
    return notModified;
  }

  public void setNotModified(boolean notModified) {
    this.notModified = notModified;
  }
}
