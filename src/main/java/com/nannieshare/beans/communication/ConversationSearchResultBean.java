package com.nannieshare.beans.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class ConversationSearchResultBean implements Serializable {

  private boolean notModified;

  private Collection<ConversationIdBean> conversations = new ArrayList<>();

  public static ConversationSearchResultBean NOT_MODIFIED() {
    ConversationSearchResultBean toReturn = new ConversationSearchResultBean();
    toReturn.setNotModified(true);
    return toReturn;
  }

  public Collection<ConversationIdBean> getConversations() {
    return conversations;
  }

  public void setConversations(Collection<ConversationIdBean> conversations) {
    this.conversations = conversations;
  }

  public boolean isNotModified() {
    return notModified;
  }

  public void setNotModified(boolean notModified) {
    this.notModified = notModified;
  }
}
