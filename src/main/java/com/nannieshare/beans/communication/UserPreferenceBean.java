package com.nannieshare.beans.communication;

import com.nannieshare.enums.user.UserType;

import java.util.ArrayList;
import java.util.List;

public class UserPreferenceBean {

  private Integer userId;
  private UserType userType;
  private Boolean active;
  private List<UserPreferenceItemBean> items = new ArrayList<>();

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public UserType getUserType() {
    return userType;
  }

  public void setUserType(UserType userType) {
    this.userType = userType;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public List<UserPreferenceItemBean> getItems() {
    return items;
  }

  public void setItems(List<UserPreferenceItemBean> items) {
    this.items = items;
  }
}
