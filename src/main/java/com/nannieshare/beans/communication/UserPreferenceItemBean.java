package com.nannieshare.beans.communication;

import com.nannieshare.enums.communication.EventType;

public class UserPreferenceItemBean {

  private EventType type;
  private Boolean email;
  private Boolean message;
  private Boolean sms;

  public EventType getType() {
    return type;
  }

  public void setType(EventType type) {
    this.type = type;
  }

  public Boolean getEmail() {
    return email;
  }

  public void setEmail(Boolean email) {
    this.email = email;
  }

  public Boolean getMessage() {
    return message;
  }

  public void setMessage(Boolean message) {
    this.message = message;
  }

  public Boolean getSms() {
    return sms;
  }

  public void setSms(Boolean sms) {
    this.sms = sms;
  }
}
