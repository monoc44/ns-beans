package com.nannieshare.beans.communication;

import com.nannieshare.enums.communication.EventType;
import com.nannieshare.enums.communication.MetaFieldType;

import java.util.HashMap;
import java.util.Map;

public class EventBean {

  private Integer id;
  private String postedTime;
  private EventType type;
  private Map<MetaFieldType, String> meta = new HashMap<>();

  public Integer getMetaAsInteger(MetaFieldType type) {
    String value = this.meta.get(type);
    return value != null ? Integer.parseInt(value) : null;
  }

  public Long getMetaAsLong(MetaFieldType type) {
    String value = this.meta.get(type);
    return value != null ? Long.parseLong(value) : null;
  }

  public Float getMetaAsFloat(MetaFieldType type) {
    String value = this.meta.get(type);
    return value != null ? Float.parseFloat(value) : null;
  }

  public Double getMetaAsDouble(MetaFieldType type) {
    String value = this.meta.get(type);
    return value != null ? Double.parseDouble(value) : null;
  }

  public void addMeta(MetaFieldType key, String value) {
    this.meta.put(key, value);
  }

  public void addMeta(MetaFieldType key, Object object) {
    String value = object != null ? object.toString() : null;
    this.meta.put(key, value);
  }

  public EventType getType() {
    return type;
  }

  public void setType(EventType type) {
    this.type = type;
  }

  public String getMeta(MetaFieldType type) {
    return this.meta.get(type);
  }

  public Map<MetaFieldType, String> getMeta() {
    return meta;
  }

  public void setMeta(Map<MetaFieldType, String> meta) {
    this.meta = meta;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getPostedTime() {
    return postedTime;
  }

  public void setPostedTime(String postedTime) {
    this.postedTime = postedTime;
  }
}
