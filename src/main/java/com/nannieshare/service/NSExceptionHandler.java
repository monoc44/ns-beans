package com.nannieshare.service;

import com.nannieshare.beans.ApiError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@ControllerAdvice
@RestController
public class NSExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(NSExceptionHandler.class);

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    List<String> errors = new ArrayList<>();
    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
      errors.add(error.getField() + ": " + error.getDefaultMessage());
    }
    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
      errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
    }

    ApiError apiError =
      new ApiError(HttpStatus.BAD_REQUEST.value(), "Bad request received (" + ex.getClass().getSimpleName() + ")", errors);

    return handleExceptionInternal(ex, apiError, headers, HttpStatus.valueOf(apiError.getStatusCode()), request);
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    String error = ex.getParameterName() + " parameter is missing";
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), error);
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ ConstraintViolationException.class })
  public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
    List<String> errors = new ArrayList<>();
    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
      errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage());
    }
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), errors);
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {
    String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), error);
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getMethod());
    builder.append(" method is not supported for this request. Supported methods are ");
    ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));
    ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED.value(), ex.getLocalizedMessage(), builder.toString());
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getContentType());
    builder.append(" media type is not supported. Supported media types are ");
    ex.getSupportedMediaTypes().forEach(t -> builder.append(t + ", "));
    ApiError apiError = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), ex.getLocalizedMessage(), builder.substring(0, builder.length() - 2));
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ AccessDeniedException.class })
  protected ResponseEntity<Object> handleAccessDenied(AccessDeniedException ex, WebRequest request) {
    ApiError apiError = new ApiError(HttpStatus.FORBIDDEN.value(), "Method not allowed for this user", ex.getMessage());
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ ServiceNotFoundException.class })
  protected ResponseEntity<Object> handleAccessDenied(ServiceNotFoundException ex, WebRequest request) {
    LOG.error(ex.getMessage());
    ApiError apiError = new ApiError(HttpStatus.BAD_GATEWAY.value(), "Please retry later as gateway could not forward your request to appropriate service", "Service not found");
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ TooManyRequestsException.class })
  public ResponseEntity<Object> handleTooManyRequests(Exception ex, WebRequest request) {
    ApiError apiError = new ApiError(
      HttpStatus.FORBIDDEN.value(), "Let's calm down... IP has sent too many requests", "Number of requested too high");
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ HttpServerErrorException.class })
  public ResponseEntity<Object> handleServiceError(HttpServerErrorException ex, WebRequest request) {
    if (ex.getStatusCode().equals(HttpStatus.SERVICE_UNAVAILABLE)) {
      ApiError apiError = new ApiError(ex.getRawStatusCode(), "This service is momentarily closed", "Please try again later");
      return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
    } else {
      ApiError apiError = new ApiError(ex.getRawStatusCode(), "Oops something went wrong on our side", "Please try again later");
      return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
    }
  }

  @ExceptionHandler({ ExecutionException.class })
  public ResponseEntity<Object> handleExecutionException(ExecutionException ex, WebRequest request) {
    if (ex.getCause() instanceof HttpServerErrorException) {
      return handleServiceError((HttpServerErrorException) ex.getCause(), request);
    } else {
      return handleAllOthers(ex.getCause(), request);
    }
  }

  @ExceptionHandler({ AccountLockedException.class })
  public ResponseEntity<Object> handleLockedAccount(AccountLockedException ex, WebRequest request) {
    List<String> errors = new ArrayList<>();
    ApiError apiError = new ApiError(HttpStatus.LOCKED.value(), "Account is locked", errors);
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ ServiceUnavailableException.class })
  public ResponseEntity<Object> handleUnavailableService(ServiceUnavailableException ex, WebRequest request) {
    List<String> errors = new ArrayList<>();
    ApiError apiError = new ApiError(HttpStatus.SERVICE_UNAVAILABLE.value(), "This service is momentarily closed", errors);
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

  @ExceptionHandler({ Throwable.class })
  public ResponseEntity<Object> handleAllOthers(Throwable ex, WebRequest request) {
    ApiError apiError = new ApiError(
      HttpStatus.INTERNAL_SERVER_ERROR.value(), "Oops, this is embarrassing... something went wrong on our side :(", ex.getClass().getName() + ": " + ex.getLocalizedMessage());
    return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.valueOf(apiError.getStatusCode()));
  }

}
