package com.nannieshare.service.recaptcha;

public class ReCaptchaException extends RuntimeException {

  public ReCaptchaException(String s) {
    super(s);
  }

  public ReCaptchaException(String s, Exception rootCause) {
    super(s, rootCause);
  }

}
