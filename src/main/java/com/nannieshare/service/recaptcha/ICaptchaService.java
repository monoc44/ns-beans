package com.nannieshare.service.recaptcha;

public interface ICaptchaService {

  void processResponse(final String response) throws ReCaptchaException;

  String getReCaptchaSite();

  String getReCaptchaSecret();
}