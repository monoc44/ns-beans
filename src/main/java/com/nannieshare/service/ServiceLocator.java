package com.nannieshare.service;

public interface ServiceLocator {

  String getServiceUrl(String serviceId, String endpoint);

}
