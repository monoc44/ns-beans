package com.nannieshare.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nannieshare.beans.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ThrottlingFilter extends GenericFilterBean {

  @Autowired
  private ThrottlingService service;

  public ThrottlingFilter() {
  }

  public ThrottlingFilter(ThrottlingService throttlingService) {
    this.service = throttlingService;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

    if (service.shouldBlock(request)) {
      block(response);

    } else {
      chain.doFilter(request, response);
    }

  }

  private void block(ServletResponse response) throws IOException {
    response.setContentType("application/json; charset=utf-8");

    if (response instanceof HttpServletResponse) {
      HttpServletResponse httpResponse = (HttpServletResponse) response;
      httpResponse.setStatus(HttpStatus.FORBIDDEN.value());
    }

    ObjectMapper mapper = new ObjectMapper();
    ApiError apiError = new ApiError(HttpStatus.FORBIDDEN.value(), "Let's calm down... IP has sent too many requests", "Number of requested too high");

    PrintWriter out = response.getWriter();
    out.print(mapper.writeValueAsString(apiError));
    out.flush();
  }
}