package com.nannieshare.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ThrottlingService {

  private static final Logger LOG = LoggerFactory.getLogger(ThrottlingService.class);

  private final int MAX_ATTEMPT = 10;
  private LoadingCache<String, AtomicInteger> attemptsCache;

  public ThrottlingService() {
    attemptsCache = CacheBuilder
      .newBuilder()
      .expireAfterWrite(1, TimeUnit.MINUTES)
      .build(new CacheLoader<String, AtomicInteger>() {
        public AtomicInteger load(String key) {
          return new AtomicInteger(0);
        }
      });
  }

  /**
   * This method is not thread-safe and two threads with same IP accessing this method
   * will conflict with each other, one overwriting the attempts atomic integer written
   * by the other one. Not a big deal as we can afford losing one or two attempts.
   *
   * We just want to make sure we control how many times an IP access free public services
   * and make sure there is no brute-force attack there on this instance.
   *
   */
  public boolean shouldBlock(ServletRequest request) {

    AtomicInteger attempts;

    String ip = getClientIP(request);

    if (ip == null) {
      LOG.warn("Could not obtain IP of request, hence could not check brute force attack");
    }

    try {
      attempts = attemptsCache.get(ip);
    } catch (ExecutionException e) {
      attempts = new AtomicInteger();
      attemptsCache.put(ip, attempts);
    }
    attempts.incrementAndGet();

    boolean blocked = false;

    try {
      blocked = attemptsCache.get(ip).get() >= MAX_ATTEMPT;
    } catch (ExecutionException e) {
      // ignored
    }

    if (blocked) {
      LOG.warn("IP {} has been requested more than {} times a service during the last minute -> has been blocked", ip, MAX_ATTEMPT);
    }

    return blocked;
  }

  private String getClientIP(ServletRequest request) {

    if (request == null) {
      return null;
    }

    String xfHeader = null;
    if (request instanceof HttpServletRequest) {
      HttpServletRequest httpRequest = (HttpServletRequest) request;
      xfHeader = httpRequest.getHeader("X-Forwarded-For");
    }

    return xfHeader == null ? request.getRemoteAddr() : xfHeader.split(",")[0];
  }

}
