package com.nannieshare.service;

import com.nannieshare.DateUtils;
import com.nannieshare.StringUtils;
import com.nannieshare.beans.user.OpeningBean;
import com.nannieshare.beans.user.OpeningStatusBean;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public final class OpeningService {

  private final LocalDate today;

  public interface OpeningLoader {
    List<OpeningBean> loadOpenings();
  }

  private final List<OpeningBean> openings;

  private OpeningService(OpeningLoader loader, LocalDate today) {
    this.openings = loader.loadOpenings();
    this.today = today;
  }

  public static final OpeningService build(OpeningLoader loader) {
    return build(loader, LocalDate.now());
  }

  public static final OpeningService build(OpeningLoader loader, LocalDate today) {
    return new OpeningService(loader, today);
  }

  /**
   * Retrieves if the given zip code is a open neighborhood.
   *
   * @param zipCode   the zip code to analyze
   * @return true if this zipCode is open to users
   */
  public OpeningStatusBean getStatus(String zipCode) {

    if (StringUtils.containsBlank(zipCode)) {
      return null;
    }

    // Check if zip code if defined in the list
    LocalDate openingDate = getOpeningDate(zipCode);

    return openAtAndAfter(openingDate);
  }

  public LocalDate getOpeningDate(String zipCode) {
    LocalDate toReturn = null;

    for (OpeningBean bean : openings) {
      if (zipCode.startsWith(bean.getZipCode())) {
        LocalDate opening = DateUtils.parseToLocalDate(bean.getOpeningDate());
        if (toReturn == null || opening.isBefore(toReturn)) {
          toReturn = opening;
        }
      }
    }

    return toReturn;
  }

  private OpeningStatusBean openAtAndAfter(LocalDate openingDate) {

    OpeningStatusBean toReturn = new OpeningStatusBean();

    LocalDate today = today();
    boolean open = false;

    if (openingDate != null) {
      open = openingDate.equals(today) || openingDate.isBefore(today);
    }

    toReturn.setOpen(open);

    if (!open && openingDate != null) {
      Long weeks = ChronoUnit.WEEKS.between(today, openingDate);
      toReturn.setEtaInWeeks(weeks.intValue());
    }

    return toReturn;
  }

  private LocalDate today() {
    return this.today != null ? this.today : LocalDate.now();
  }
}
