package com.nannieshare.service;

import com.nannieshare.beans.user.PublicUserBean;
import com.nannieshare.enums.user.RoleType;
import com.nannieshare.enums.user.UserType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import java.util.*;

/**
 *
 * Typical JWT custom claims payload:
 *
 *  userId': '971f7d6b-bf82-43d6-ad85-fcd3e1eb0544'
 *  lastName': 'Ville'
 *  firstName': 'Frederic'
 *  user_name': 'frederic.ville@gmail.com'
 *  email': 'frederic.ville@gmail.com'
 *  roles': '[basic, admin]'
 *  type: 'parent'
 *  exp': '1523518026'
 *  aud': '[nannieshare]'
 *  scope': '[read, write]'
 *  jti': '1b593af9-0ada-454c-adea-a9218e748219'
 *  client_id': 'NS_JTW_CLIENT'
 *  authorities': '[parent, basic]'
 *
 */
public final class Auth {

  private static final String USER_ID = "userId";
  private static final String ROLES = "roles";
  private static final String TYPE = "type";
  private static final String EMAIL = "email";
  private static final String EXPIRATION = "exp";

  private SecurityContext securityContext;
  private Authentication authentication;
  private OAuth2AuthenticationDetails oauthDetails;
  private Map<String, Object> decodedDetails;

  public Auth() {

    securityContext = SecurityContextHolder.getContext();

    if (securityContext != null) {
      authentication = securityContext.getAuthentication();
    }

    if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
      oauthDetails = (OAuth2AuthenticationDetails) authentication.getDetails();
      Objects.requireNonNull(oauthDetails, "Odd, missing OAuth details, should be there... token corrupted?");

      decodedDetails = (Map<String, Object>) oauthDetails.getDecodedDetails();
      Objects.requireNonNull(decodedDetails, "Odd, missing OAuth decoded details, should be there... token corrupted?");
    }
  }

  public static final Auth get() {
    return new Auth();
  }

  public boolean isJWTSecured() {
    return securityContext != null && authentication != null && oauthDetails != null && decodedDetails != null;
  }

  public static final Integer userId() {
    return new Auth().getUserId();
  }

  public static final boolean isUserPremium() {
    return new Auth().getRoles().contains(RoleType.PREMIUM);
  }

  public SecurityContext getSecurityContext() {
    return securityContext;
  }

  public Integer getUserId() {
    return (Integer) decodedDetails.get(USER_ID);
  }

  public String getTokenType() {
    return oauthDetails.getTokenType();
  }

  public String getTokenValue() {
    return oauthDetails.getTokenValue();
  }

  public Date getExpirationDate() {
    Long expireTs = (Long) decodedDetails.get(EXPIRATION);
    // Timestamp appears to be a seconds-based epoch value
    return new Date(expireTs * 1000);
  }

  public String getEmail() {
    return (String) decodedDetails.get(EMAIL);
  }

  public UserType getUserType() {
    String type = (String) decodedDetails.get(TYPE);
    return UserType.from(type);
  }

  public PublicUserBean getUserIdBean() {
    PublicUserBean toReturn = new PublicUserBean();
    toReturn.setUserId(getUserId());
    toReturn.setType(getUserType());
    return toReturn;
  }

  public List<RoleType> getRoles() {
    List<RoleType> toReturn = new ArrayList<>();
    for (String role : (List<String>) decodedDetails.get(ROLES)) {
      toReturn.add(RoleType.from(role));
    }
    return toReturn;
  }

}
