package com.nannieshare.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public final class ControllerInvoker {

  private static final Logger LOG = LoggerFactory.getLogger(ControllerInvoker.class);

  private RestTemplate restTemplate = buildRestTemplate();

  private ExecutorService executorService = Executors.newCachedThreadPool();

  private ServiceLocator serviceLocator;

  public ControllerInvoker(ServiceLocator serviceLocator) {
    Objects.requireNonNull(serviceLocator, "Service locator is missing, can't use this service");
    this.serviceLocator = serviceLocator;
  }

  /**
   * Helper to request a GET service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> ResponseEntity<O> get(String serviceId, String endpoint, Class<O> outputType,  Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.GET, buildHttpEntity(), outputType, uriVariables);
  }

  /**
   * Helper to request a GET service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param headers the headers to add to this request
   * @param uriVariables the URI variables
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> ResponseEntity<O> get(String serviceId, String endpoint, Class<O> outputType, Map<String, String> headers, Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.GET, buildHttpEntity(headers), outputType, uriVariables);
  }

  /**
   * Helper to request asynchronously a GET service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> Future<ResponseEntity<O>> getAsync(Auth auth, String serviceId, String endpoint, Class<O> outputType,  Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return get(serviceId, endpoint, outputType, uriVariables);
    });
  }

  /**
   * Helper to request a GET service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <O> the list of output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> ResponseEntity<List<O>> getList(String serviceId, String endpoint, ParameterizedTypeReference<List<O>> outputType,  Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchangeForList(url, HttpMethod.GET, buildHttpEntity(), outputType, uriVariables);
  }

  /**
   * Helper to request a GET service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <O> the list of output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> Future<ResponseEntity<List<O>>> getListAsync(Auth auth, String serviceId, String endpoint, ParameterizedTypeReference<List<O>> outputType,  Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return getList(serviceId, endpoint, outputType, uriVariables);
    });
  }

  /**
   * Helper to request a POST service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<O> post(String serviceId, String endpoint, I payload, Class<O> outputType,  Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.POST, buildHttpEntity(payload, null), outputType, uriVariables);
  }

  /**
   * Helper to request a POST service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param headers the headers to add to this request
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<O> post(String serviceId, String endpoint, I payload, Class<O> outputType, Map<String, String> headers, Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.POST, buildHttpEntity(payload, headers), outputType, uriVariables);
  }

  /**
   * Helper to request asynchronously a POST service on a service/endpoint target
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> Future<ResponseEntity<O>> postAsync(Auth auth, String serviceId, String endpoint, I payload, Class<O> outputType,  Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return post(serviceId, endpoint, payload, outputType, uriVariables);
    });
  }

  /**
   * Helper to request a POST service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<List<O>> postForList(String serviceId, String endpoint, I payload, ParameterizedTypeReference<List<O>> outputType,  Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchangeForList(url, HttpMethod.POST, buildHttpEntity(payload, null), outputType, uriVariables);
  }

  /**
   * Helper to request a POST service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> Future<ResponseEntity<List<O>>> postForListAsync(Auth auth, String serviceId, String endpoint, I payload, ParameterizedTypeReference<List<O>> outputType,  Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return postForList(serviceId, endpoint, payload, outputType, uriVariables);
    });
  }

  /**
   * Helper to request a PUT service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param headers the headers to add to this request
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<O> put(String serviceId, String endpoint, I payload, Class<O> outputType, Map<String, String> headers, Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.PUT, buildHttpEntity(payload, headers), outputType, uriVariables);
  }

  /**
   * Helper to request a PUT service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<O> put(String serviceId, String endpoint, I payload, Class<O> outputType,  Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.PUT, buildHttpEntity(payload, null), outputType, uriVariables);
  }

  /**
   * Helper to request asynchronously a PUT service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> Future<ResponseEntity<O>> putAsync(Auth auth, String serviceId, String endpoint, I payload, Class<O> outputType,  Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return put(serviceId, endpoint, payload, outputType, uriVariables);
    });
  }

  /**
   * Helper to request a PUT service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<List<O>> putForList(String serviceId, String endpoint, I payload, ParameterizedTypeReference<List<O>> outputType,  Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchangeForList(url, HttpMethod.PUT, buildHttpEntity(payload, null), outputType, uriVariables);
  }

  /**
   * Helper to request asynchronously a PUT service on a service/endpoint target
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> Future<ResponseEntity<List<O>>> putForListAsync(Auth auth, String serviceId, String endpoint, I payload, ParameterizedTypeReference<List<O>> outputType,  Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return putForList(serviceId, endpoint, payload, outputType, uriVariables);
    });
  }

  /**
   * Helper to request a DELETE service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> ResponseEntity<O> delete(String serviceId, String endpoint, Class<O> outputType, Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.DELETE, buildHttpEntity(), outputType, uriVariables);
  }

  /**
   * Helper to request a DELETE service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> ResponseEntity<O> delete(String serviceId, String endpoint, I payload, Class<O> outputType, Object... uriVariables) {
    String url = getServiceUrl(serviceId, endpoint);
    return exchange(url, HttpMethod.DELETE, buildHttpEntity(payload, null), outputType, uriVariables);
  }

  /**
   * Helper to request asynchronously a DELETE service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <O> Future<ResponseEntity<O>> deleteAsync(Auth auth, String serviceId, String endpoint, Class<O> outputType, Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return delete(serviceId, endpoint, outputType, uriVariables);
    });
  }

  /**
   * Helper to request asynchronously a DELETE service on a service/endpoint target.
   * @param serviceId the targeted service ID
   * @param endpoint the endpoint targeted on this service
   * @param payload the payload of the request
   * @param outputType the expected result class type
   * @param uriVariables the URI variables
   * @param <I> the input serializable type class
   * @param <O> the output serializable type class
   * @return a {@link ResponseEntity}, response obtained from targeted service
   */
  public <I, O> Future<ResponseEntity<O>> deleteAsync(Auth auth, String serviceId, String endpoint, I payload, Class<O> outputType, Object... uriVariables) {
    return executorService.submit(() -> {
      SecurityContextHolder.setContext(auth.getSecurityContext());
      return delete(serviceId, endpoint, payload, outputType, uriVariables);
    });
  }

  // *****************************************************************************
  // PRIVATE METHODS
  // *****************************************************************************

  private String getServiceUrl(String serviceId, String endpoint) {
    return serviceLocator.getServiceUrl(serviceId, endpoint);
  }

  private RestTemplate buildRestTemplate() {
    RestTemplate toReturn = new RestTemplate();
    toReturn.setErrorHandler(new CustomErrorHandler());
    return toReturn;
  }

  private <O> ResponseEntity<O> exchange(String url, HttpMethod method, HttpEntity httpEntity, Class<O> outputType, Object[] uriVariables) {
    LOG.debug("Sending {} request to service URL: '{}'", method, url);
    for (Object uriVariable : uriVariables) {
      LOG.debug("with uri: {}", uriVariable);
    }
    ResponseEntity<O> toReturn = restTemplate.exchange(url, method, httpEntity, outputType, uriVariables);
    LOG.debug("Response received, status={}", toReturn.getStatusCode());
    return toReturn;
  }

  private <O> ResponseEntity<List<O>> exchangeForList(String url, HttpMethod method, HttpEntity httpEntity, ParameterizedTypeReference<List<O>> output, Object[] uriVariables) {
    LOG.debug("Sending {} request to service URL: '{}'", method, url);
    for (Object uriVariable : uriVariables) {
      LOG.debug("with uri: {}", uriVariable);
    }
    ResponseEntity<List<O>> toReturn = restTemplate.exchange(url, method, httpEntity, output, uriVariables);
    LOG.debug("Response received, status={}", toReturn.getStatusCode());
    return toReturn;
  }

  private HttpEntity buildHttpEntity() {
    return buildHttpEntity(null, null);
  }

  private HttpEntity buildHttpEntity(Map<String, String> withHeaders) {
    return buildHttpEntity(null, withHeaders);
  }

  private HttpEntity buildHttpEntity(Object body, Map<String, String> withHeaders) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

    if (withHeaders != null) {
      for (Entry<String, String> entry : withHeaders.entrySet()) {
        headers.set(entry.getKey(), entry.getValue());
      }
    }

    // Add JWT token if connection is secured with JWT
    Auth auth = Auth.get();
    if (auth.isJWTSecured()) {
      headers.set("Authorization", auth.getTokenType() + " " + auth.getTokenValue());
    }
    return body == null ? new HttpEntity(headers) : new HttpEntity(body, headers);
  }


  private class CustomErrorHandler extends DefaultResponseErrorHandler {
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
      LOG.warn("Service returned with an error and status code {}", response.getRawStatusCode());
      if (response.getStatusCode().equals(HttpStatus.LOCKED)) {
        throw new AccountLockedException();
      }
      if (response.getStatusCode().equals(HttpStatus.SERVICE_UNAVAILABLE)) {
        throw new ServiceUnavailableException();
      }
      if (response.getStatusCode().is5xxServerError()) {
        super.handleError(response);
      }
    }
  }
}
