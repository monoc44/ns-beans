package com.nannieshare.service;

public class ServiceNotFoundException extends RuntimeException {

  public ServiceNotFoundException(String serviceId) {
    super("No service found matching with name: " + serviceId);
  }
}
